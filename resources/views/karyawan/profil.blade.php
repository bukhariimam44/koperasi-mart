@extends('layouts.karyawan.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">

<div class="row">
<div class="col-xs-4">
  <h4 class="page-title">Profil</h4>
</div>
<div class="col-xs-8 text-right m-b-30">
  <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#edit_profil"><i class="fa fa-edit"></i> Edit Profil</a>
  <div class="view-icons">
    <!-- <a href="{{url('/admin/data-pegawai')}}" class="grid-view btn btn-link"><i class="fa fa-th"></i></a> -->

  </div>
</div>
</div>
<div class="card-box m-b-0">
<div class="row">
  <div class="col-md-12">
    <div class="profile-view">
      <div class="profile-img-wrap">
        <div class="profile-img">
          <a href=""><img class="avatar" src="{{ asset('foto/'.Auth::user()->fotodiri) }}" alt=""></a>
        </div>
      </div>
      <div class="profile-basic">
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table">
                <tr>
                  <td>Nomor Karyawan</td>
                  <td>: {{ Auth::user()->no_anggota }}</td>
                </tr>
                <tr>
                  <td>Nama Lengkap</td>
                  <td>: {{ Auth::user()->name }}</td>
                </tr>
                <tr>
                  <td>Jenis Kelamin</td>
                  <td>: {{ Auth::user()->jenkel }}</td>
                </tr>
                <tr>
                  <td>Tanggal Lahir</td>
                  <td>: {{ Auth::user()->tgl_lahir }}</td>
                </tr>
                <tr>
                  <td>Email</td>
                  <td>: <a href="">{{ Auth::user()->email }}</a></td>
                </tr>
                <tr>
                  <td>Nomor Telpon</td>
                  <td>: {{ Auth::user()->telp }}</td>
                </tr>
                <tr>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<div class="card-box tab-box">
<!-- <div class="row user-tabs">
  <div class="col-lg-12 col-md-12 col-sm-12 line-tabs">
    <ul class="nav nav-tabs tabs nav-tabs-bottom"> -->
      <!-- <li class="active col-sm-3"><a data-toggle="tab" href="#myprojects">My Detail</a></li> -->
      <!-- <li class="active col-sm-3"><a data-toggle="tab" href="#tasks">Riwayat</a></li>
    </ul>
  </div>
</div> -->
</div>

<div id="edit_profil" class="modal custom-modal fade" role="dialog">
  <div class="modal-dialog">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h4 class="modal-title">Edit Profil</h4>
      </div>
      <div class="modal-body">
        <form class="m-b-30" action="{{url('/karyawan/profil')}}" method="post" enctype="multipart/form-data">
          @csrf
          <input type="hidden" name="action" value="edit">
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">No. Karyawan<span class="text-danger">*</span></label>
                <input class="form-control" type="text" name="no_anggota" value="{{Auth::user()->no_anggota}}" disabled>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Nama Lengkap<span class="text-danger">*</span></label>
                <input class="form-control" type="text" name="name" value="{{Auth::user()->name}}" disabled>
              </div>
            </div>

            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Nomor Telpon </label>
                <input class="form-control" type="text" name="telp" value="{{Auth::user()->telp}}">
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Tanggal Lahir </label>
                <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_lahir" value="{{date('d-m-Y', strtotime(Auth::user()->tgl_lahir))}}"></div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Jenis Kelamin </label>
                <select class="select" name="jenkel">
                  <?php $jenkel = ['Laki-laki','Perempuan'] ;?>
                  <option value="">Pilih Jenis Kelamin</option>
                  @foreach($jenkel as $jen)
                  @if(Auth::user()->jenkel == $jen)
                  <option value="{{$jen}}" selected>{{$jen}}</option>
                  @else
                  <option value="{{$jen}}">{{$jen}}</option>
                  @endif
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Foto Diri 4x6<span class="text-danger">*</span></label>
                <input class="form-control" type="file" name="fotodiri">
              </div>
            </div>
          </div>

          <div class="m-t-20 text-center">
            <button class="btn btn-primary">SIMPAN</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

  </div>
</div>


</div>
</div>
@endsection
