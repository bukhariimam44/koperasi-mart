@extends('layouts.pengurus.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">

<div class="row">
  <div class="col-md-6 col-sm-6 col-lg-3">
		<div class="dash-widget clearfix card-box">
			<span class="dash-widget-icon"><i class="fa fa-child fa-lg" aria-hidden="true"></i></span>
			<div class="dash-widget-info">
				<h3><?php $user = App\User::where('type','anggota')->get();
        echo count($user);?></h3>
				<span>Jumlah Anggota</span>
			</div>
		</div>
	</div>

  <div class="col-md-6 col-sm-6 col-lg-3">
		<div class="dash-widget clearfix card-box">
			<span class="dash-widget-icon"><i class="fa fa-child fa-lg" aria-hidden="true"></i></span>
			<div class="dash-widget-info">
				<h3><?php $user = App\User::where('type','pengurus')->get();
        echo count($user);?></h3>
				<span>Jumlah Pengurus</span>
			</div>
		</div>
	</div>

  <div class="col-md-6 col-sm-6 col-lg-3">
		<div class="dash-widget clearfix card-box">
			<span class="dash-widget-icon"><i class="fa fa-child fa-lg" aria-hidden="true"></i></span>
			<div class="dash-widget-info">
				<h3><?php $user = App\User::where('type','admin')->get();
        echo count($user);?></h3>
				<span>Jumlah Administrator</span>
			</div>
		</div>
	</div>

	<div class="col-md-6 col-sm-6 col-lg-3">
		<div class="dash-widget clearfix card-box">
			<span class="dash-widget-icon"><i class="fa fa-group fa-lg" aria-hidden="true"></i></span>
			<div class="dash-widget-info">
				<h3><?php $user = App\User::where('type','<>','karyawan')->get();
        echo count($user);?></h3>
				<span>Total Seluruh Anggota</span>
			</div>
		</div>
	</div>
</div>


<div class="row">
	<div class="col-xs-4">
		<h4 class="page-title">Pengumuman Terbaru</h4>
	</div>

	<div class="col-xs-8 text-right m-b-30">
    <!-- <a href="#" class="btn btn-primary rounded pull-right" data-toggle="modal" data-target="#create_project"><i class="fa fa-plus"></i> Tambah Pengumuman</a> -->
		<!-- <div class="view-icons">
			<a href="#" class="list-view btn btn-link active"><i class="fa fa-bars active"></i></a>
		</div> -->
	</div>
</div>

<div class="row">

  <?php $berita = App\Berita::where('aktif',1)->get(); ?>
  @foreach($berita as $b)
  <div class="col-lg-12 col-sm-6">
  	<div class="card-box project-box">
  		<h4 class="project-title"><a href="#">{{$b->judul}}</a></h4>
  		<small class="block text-ellipsis m-b-15"></small>
  		{!!$b->berita!!}

  	<div class="pro-deadline m-b-15">
      <hr>
  		<div class="sub-title">Date Post: </div>
  		<div class="text-muted">{{$b->updated_at}}</div>
      <div class="text-muted"></div>
  	</div>
  </div>
  </div>
  @endforeach
  <div class="themes">
    <div class="themes-icon"><i class="fa fa-cog"></i></div>
    <div class="themes-body">
      <ul id="theme-change" class="theme-colors">
        <li><span class="theme-orange"></span></li>
        <li><span class="theme-purple"></span></li>
        <li><span class="theme-blue"></span></li>
        <li><span class="theme-maroon"></span></li>
        <li><span class="theme-light"></span></li>
        <li><span class="theme-dark"></span></li>
      </ul>
    </div>
  </div>

</div>
</div>
            <!-- CREATE INFORMASI -->
			<div id="create_project" class="modal custom-modal fade" role="dialog">
				<div class="modal-dialog">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<div class="modal-content modal-lg">
						<div class="modal-header">
							<h4 class="modal-title">Tambah Pengumuman </h4>
						</div>
						<div class="modal-body">
							<form id="create_project2" action="{{url('/admin/create-info')}}" method="post">
                @csrf
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label>Judul</label>
											<input class="form-control" type="text" name="title">
										</div>
									</div>
								</div>
                <div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label>Di Tujukan Kepada</label>
											<select class="form-control" name="tertuju" required>
                        <option value="all">Semua</option>
                        <option value="admin">Admin</option>
                        <option value="kadinas">Kadinas</option>
                        <option value="pegawai">Pegawai</option>
                        <option value="anggota">Anggota DPD</option>
                      </select>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label>Deskripsi</label>
									<textarea rows="4" cols="5" class="form-control summernote" placeholder="Enter your message here" name="description"></textarea>
								</div>
								<div class="m-t-20 text-center">
									<button class="btn btn-primary" type="submit">Create Information</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
      <!-- ENDCREATE INFORMASI -->


			<div id="edit_info" class="modal custom-modal fade" role="dialog">
				<div class="modal-dialog">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<div class="modal-content modal-lg">
						<div class="modal-header">
							<h4 class="modal-title">Edit Information</h4>
						</div>
            <form action="{{url('/admin/info')}}" method="post">
              @csrf
						<div class="modal-body">
                <div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label>Title</label>
											<input type="text" class="form-control" name="judul" value="">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label>Description</label>
									<textarea name="berita_terbaru" rows="4" cols="5" class="form-control summernote" placeholder="Enter your message here">ISI BERITA</textarea>
								</div>
								<div class="m-t-20 text-center">
									<input type="submit" class="btn btn-primary" value="Save Changes"/>
								</div>

						</div>
            </form>
					</div>
				</div>
			</div>

      <!-- ENDEDITINFO -->
      <form class="" action="{{'/admin/delete-info'}}" method="post">
        @csrf
        <div id="hapus_info" class="modal custom-modal fade" role="dialog">
  				<div class="modal-dialog">
  					<div class="modal-content modal-md">
  						<div class="modal-header">
  							<h4 class="modal-title">Hapus Informasi</h4>
  						</div>
  						<div class="modal-body card-box">
  							<p>Anda yakin ingin menghapus Informasi :</p>
                <p>  ???</p>
  							<div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
  								<button type="submit" class="btn btn-danger">Delete</button>
  							</div>
  						</div>
  					</div>
  				</div>
  			</div>
      </form>


        </div>
		<div class="sidebar-overlay" data-reff="#sidebar"></div>
@endsection
