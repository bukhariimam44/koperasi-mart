@extends('layouts.pengurus.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">

<div class="row">
<div class="col-xs-4">
  <h4 class="page-title">Data Anggota</h4>
</div>
<div class="col-xs-8 text-right m-b-30">
  <!-- <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_employee"><i class="fa fa-plus"></i> Tambah Anggota</a> -->
  <div class="view-icons">
    <!-- <a href="{{url('/admin/data-pegawai')}}" class="grid-view btn btn-link"><i class="fa fa-th"></i></a> -->

  </div>
</div>
</div>
<div class="row filter-row">
  <form class="form" action="{{url('/pengurus/data-anggota')}}" method="post">
    @csrf
    <input type="hidden" name="action" value="cari">
    <div class="col-sm-4 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">No. Anggota</label>
        <input type="text" class="form-control floating" name="no_anggota" value="{{$no_anggota}}"/>
      </div>
    </div>
    <div class="col-sm-4 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Nama Anggota</label>
        <input type="text" class="form-control floating" name="name" value="{{$nama}}"/>
      </div>
    </div>
    <div class="col-sm-4 col-xs-12">
      <input type="submit" class="btn btn-success btn-block" name="btn" value="TAMPILKAN"/>
    </div>
  </form>
</div><a href="{{url('/admin/data-pegawai')}}" class="pull-right"><img width="30px" src="{{url('/images/excel.png')}}" onclick="event.preventDefault();
              document.getElementById('export').submit();"/></a>
<div class="row">
  <form class="" action="{{('#')}}" method="post" id="export">
    @csrf
    <input type="hidden" name="nip" value=""/>
    <input type="hidden" name="hakakses" value=""/>
    <input type="hidden" name="jabatan" value=""/>
    <input type="hidden" name="export" value="1"/>
  </form>

<div class="col-md-12">

  <div class="table-responsive">

    <table class="table table-striped custom-table datatable">
      <thead>
        <tr>
          <th>No.</th>
          <th>No-KSSD</th>
          <th>Nama</th>
          <th>No-KS212</th>
          <th>Saldo</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <?php $no=1; ?>
        @foreach($user as $us)
        <tr>
          <td>{{$no++}}.</td>
          <td>{{$us->no_anggota}}</td>
          <td>{{$us->name}}</td>
          <td>{{$us->no_ks212}}</td>
          <td>Rp. {{number_format($us->saldo)}}.-</td>
          <td style="max-width:80px;" class="text-right">
            <a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#detail{{$us->id}}">Detail</a>
          </td>
        </tr>
        @endforeach
        @if(count($user) < 1)
        <tr>
          <td colspan="8" class="text-center">KOSONG</td>
        </tr>
        @endif
      </tbody>

    </table>
  </div>
</div>
</div>
    </div>

    @foreach($user as $use)
    <div id="detail{{$use->id}}" class="modal custom-modal fade" role="dialog">
  <div class="modal-dialog">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h4 class="modal-title">Detail Data Anggota</h4>
      </div>
      <div class="modal-body">
          <table class="table">
            <tr>
              <td colspan="2" align="center"><img src="{{asset('foto/'.$use->fotodiri)}}" width="150px"/></td>
            </tr>
            <tr>
              <td>Nomor Anggota</td>
              <td>: {{$use->no_anggota}}</td>
            </tr>
            <tr>
              <td>Nomor KS212</td>
              <td>: {{$use->no_ks212}}</td>
            </tr>
            <tr>
              <td>Nama Lengkap</td>
              <td>: {{$use->name}}</td>
            </tr>
            <tr>
              <td>Email</td>
              <td>: {{$use->email}}</td>
            </tr>
            <tr>
              <td>Nomor Telpon</td>
              <td>: {{$use->telp}}</td>
            </tr>
            <tr>
              <td>No.NPWP</td>
              <td>: {{$use->npwp}}</td>
            </tr>
            <tr>
              <td>Tempat Lahir</td>
              <td>: {{$use->tpt_lahir}}</td>
            </tr>
            <tr>
              <td>Tanggal Lahir</td>
              <td>: {{date('d-m-Y', strtotime($use->tgl_lahir))}}</td>
            </tr>
            <tr>
              <td>Jenis Kelamin</td>
              <td>: {{$use->jenkel}}</td>
            </tr>
            <tr>
              <td>Agama</td>
              <td>: {{$use->agama}}</td>
            </tr>
            <tr>
              <td>Pendidikan</td>
              <td>: @if($use->pendidikan > 0){{$use->pendidikan_id->name}} @else  @endif</td>
            </tr>
            <tr>
              <td>Status dalam keluarga</td>
              <td>: {{$use->statuskeluarga}}</td>
            </tr>
            <tr>
              <td>Alamat</td>
              <td>: {{$use->alamat}}</td>
            </tr>
            <tr>
              <td>Kelurahan</td>
              <td>: @if($use->kelurahan > 0){{ $use->kelurahan_id->name }} @else  @endif</td>
            </tr>
            <tr>
              <td>Kecamatan </td>
              <td>: @if($use->kecamatan > 0){{ $use->kecamatan_id->name }} @else  @endif</td>
            </tr>
            <tr>
              <td>Kabupaten/Kota</td>
              <td>: @if($use->kabupaten > 0){{ $use->kabupaten_id->name }} @else  @endif</td>
            </tr>
            <tr>
              <td>Propinsi </td>
              <td>: @if($use->propinsi > 0){{ $use->propinsi_id->name }} @else @endif </td>
            </tr>
            <tr>
              <td><strong>SALDO</strong> </td>
              <td><strong>: Rp. {{ number_format($use->saldo) }},-</strong></td>
            </tr>
            <tr>
              <td colspan="2"></td>
            </tr>
          </table>
      </div>
    </div>
  </div>
</div>
@endforeach

      </div>
  <div class="sidebar-overlay" data-reff="#sidebar"></div>
@endsection
