@extends('layouts.pengurus.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">
<div class="row">
<div class="col-xs-6">
  <h4 class="page-title">Pemesanan</h4>
</div>
<div class="col-xs-6">
  <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#catatan"> Catatan</a>
</div>

</div>
<div class="row">
<div class="col-md-4">
  <div class="profile-widget">
    <div class="profile-imges">
      <img src="{{url('laravel/public/gambars/'.$produk->gambar)}}" width="100%">
    </div>

    <br>
    <div class="profile-imges">
      <h4>Detail Barang</h4><!-- <a href="#"  data-toggle="modal" data-target="#lihat"><img src="{{url('laravel/public/gambars/'.$produk->gambar)}}" width="100%"></a> -->
    </div><br>
    <p><h6 class="user-name m-t-10 m-b-0 text-left">Kode : {{$produk->kode}}</h6></p>
    <h6 class="user-name m-t-10 m-b-0 text-left">Nama : {{$produk->name}}</h6>
    <h6 class="user-name m-t-10 m-b-0 text-left">Harga : Rp {{number_format($produk->harga,0,",",".")}}</h6>
    <h6 class="user-name m-t-10 m-b-0 text-left">Berat : {{$produk->berat}} Gram / {{$produk->berat/1000}} Kg</h6>
    <h6 class="user-name m-t-10 m-b-0 text-left">Stok : {{$produk->stok}}</h6>
    <h6 class="user-name m-t-10 m-b-0 text-left">Keterangan : {{$produk->keterangan}}</h6>
    <div class="small text-muted"></div>
  </div>
</div>

<div class="col-md-8">
  <div class="profile-widget">

    <form class="" action="{{route('pengurus-bayar')}}" method="post" id="beli">
      @csrf
      <div class="profile-imges">
        <h4>Pemesanan</h4><!-- <a href="#"  data-toggle="modal" data-target="#lihat"><img src="{{url('laravel/public/gambars/'.$produk->gambar)}}" width="100%"></a> -->
      </div>
    <h6 class="user-name m-t-10 m-b-0 text-left">Jumlah :
    <select name="jumlah" id="jumlah">
      <?php for ($i=1; $i < $produk->stok+1; $i++) {
          echo "<option value=".$i.">".$i."</option>";
      } ?>
    </select> </h6>

    <input type="hidden" name="ids" id="ids" value="{{$produk->id}}">

    <script type="text/javascript">

                $("select[name='jumlah']").change(function(){
                    $('.modal').modal('show');
                    var jumlah = $(this).val();
                    var ids = $("#ids").val();
                    var token = $("input[name='_token']").val();
                    $.ajax({
                        url: "<?php echo route('pengurus-select-jumlah') ?>",
                        method: 'POST',
                        data: {ids:ids,jumlah:jumlah, _token:token},
                        success: function(data) {
                          $('.modal').modal('hide');
                          console.log(data);
                          $("#id_harga").html("");
                          $("#city").html("");
                          $("#id_harga").append(data);
                          $("#divharga").hide();
                          $("#send").hide();

                        }
                    });
                });
              </script>
              <h6 class="user-name m-t-10 m-b-0 text-left">Total Harga : <label name="id_harga" id="id_harga">Rp. {{number_format($produk->harga,0,",",".")}}</label></h6><hr>
              <h6 class="user-name m-t-10 m-b-0 text-left">Kirim Dari : Cilegon</h6>
              <h6 class="user-name m-t-10 m-b-0 text-left">Kirim Ke : <br><br>
                <select style="width:100%;" name="province" id="province" required>
                  <option value="">Pilih Propinsi</option>
                  <?php foreach ($detail as $key => $value) {
    echo "<option value=".$value['province_id'].">".$value['province']."</option>";
};?>
                </select>

          <br>
          <center> <div id="wait" style="display:none;width:50%;border:0px solid black;position:absolute;top:50%;padding:5px;"><img src="{{url('images/load.gif')}}" width="100%" /></div></center>
          <input type="hidden" name="berat" value="{{$produk->berat}}" id="berat">
<script type="text/javascript">

            $("select[name='province']").change(function(){
              $('.modal').modal('show');
                var province = $(this).val();
                var token = $("input[name='_token']").val();
                $.ajax({
                    url: "<?php echo route('pengurus-select-province') ?>",
                    method: 'POST',
                    data: {province:province, _token:token},
                    success: function(data) {
                      console.log(data);
                      $("#city").html("");
                      $("#city").append(data);
                      $("#divharga").hide();
                      $("#send").hide();
                        $('.modal').modal('hide');
                    }
                });
            });
          </script>
    <br>
    <select style="width:100%;" name="city" id="city" required>
      <option value="">Pilih Kota</option>
    </select>
      <script type="text/javascript">

                  $("select[name='city']").change(function(){
                    $('.modal').modal('show');
                      var city = $(this).val();
                      var jumlah = $("#jumlah").val();
                      var berat = $("#berat").val();
                      var token = $("input[name='_token']").val();
                      $.ajax({
                          url: "<?php echo route('pengurus-select-city') ?>",
                          method: 'POST',
                          data: {jumlah:jumlah,berat:berat,city:city, _token:token},
                          success: function(data) {
                            console.log('success');
                            console.log(data);
                            $("#detail").html("");
                            $("#detail").append(data);
                            $("#divharga").show();
                            $("#send").show();
                              $('.modal').modal('hide');
                          }
                      });
                  });
                </script>
                <br><br>
                <textarea name="alamat" rows="3" style="width:100%;" value="" placeholder="Alamat Lengkap (Jln, RT RW, Telp Pengirim & Penerima)" required></textarea>
              </h6><br>
              <h6><div id="send">
                <select class="" name="kirim" id="kirim"  style="width:100%;">
                  <option value="jne">Melalui : JNE</option>
                  <option value="tiki">Melalui : TIKI</option>
                  <option value="pos">Melalui : POS</option>
                </select>
              </div></h6>

              <script>
                $("#send").hide();
              </script>
    <script type="text/javascript">

                $("select[name='kirim']").change(function(){
                  $('.modal').modal('show');
                    var kirim = $(this).val();
                    var city = $("#city").val();
                    var jumlah = $("#jumlah").val();
                    var berat = $("#berat").val();
                    var token = $("input[name='_token']").val();
                    $.ajax({
                        url: "<?php echo route('pengurus-select-kirim') ?>",
                        method: 'POST',
                        data: {kirim:kirim,jumlah:jumlah,berat:berat,city:city, _token:token},
                        success: function(data) {
                          console.log('success');
                          console.log(data);
                          $("#detail").html("");
                          $("#detail").append(data);
                          $("#divharga").show();
                          $("#send").show();
                          $('.modal').modal('hide');
                        }
                    });
                });
              </script>

              <div id="divharga">
                <h6 class="text-left" id="detail" name="detail">DETAIL</h6>
                <hr>
                <input type="hidden" name="action" value="beli">
                <input type="hidden" name="ids" value="{{$produk->id}}">
                <p> <label for="setuju"><input type="checkbox" name="setuju" value="1" required id="setuju"> Setujui saldo di potong otomatis !</label> </p>
                <!-- <input type="submit" name="" class="btn btn-primary btn-sm m-t-10" value="LANJUTKAN"> -->
                <a href="#" onclick="event.preventDefault();
                              document.getElementById('beli').submit();" class="btn btn-primary btn-sm m-t-10">PEMBAYARAN</a>
                <a href="#" data-toggle="modal" data-target="#detail{{$produk->id}}" class="btn btn-danger btn-sm m-t-10">BATAL</a>
              </div>
              <script>
                $("#divharga").hide();
              </script>

              </form>
            </div>
          </div>

          <!-- MODAL LOADING -->
          <style>
          .bd-example-modal-lg .modal-dialog{
            display: table;
            position: relative;
            margin: 0 auto;
            text-align:center;
            top: calc(50% - 24px);
          }
          .bd-example-modal-lg .modal-dialog .modal-content{
            background-color: #ffffff;
            border: none;
          }
          </style>
          <div class="modal fade bd-example-modal-lg" data-backdrop="static" data-keyboard="false" tabindex="-1">
              <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                      <span class="fa fa-spinner fa-spin fa-5x"></span><br><br>
                      Mohon Menunggu...
                  </div>

              </div>
          </div>
          <!-- MODAL -->

</div>
</div>
</div>
<div class="sidebar-overlay" data-reff="#sidebar"></div>
<script type="text/javascript">
function bayarKes()	{
  $('#pilihBank').slideUp("fast");
  document.getElementById("pilihBank").required = false;
}
function bayarTransfer()	{
  $('#pilihBank').slideDown("fast");
  document.getElementById("pilihBank").required = true;
}
</script>
@endsection
