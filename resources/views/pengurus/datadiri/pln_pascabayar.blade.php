@extends('layouts.pengurus.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <h4 class="page-title">PLN PASCABAYAR</h4>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-8 col-md-6 col-xs-12" id="respon">
          <div class="" id="message"></div>
        </div>
        <script>
          $("#respon").hide();
        </script>
        <div class="col-sm-8 col-md-6 col-xs-12" id="form">
          <div class="" id="errors"></div>
          <script>
            $("#errors").hide();
          </script>
          <form class="" action="{{url('/pengurus/proses-ppob')}}" method="post">
            @csrf
          <div class="form-group" id="nomor">
            <input type="text" name="nomor" class="form-control" value="" placeholder="Nomor Meteran" required>
          </div>
          <div class="form-group" id="buttom">
            <input type="button" class="form-control btn btn-primary" id="btn-proses" name="btn" value="CEK TAGIHAN">
          </div>
          <script type="text/javascript">
            $("#btn-proses").on("click", function(){
              $('.modal').modal('show');
                var nomor = $("input[name='nomor']").val();
                var token = $("input[name='_token']").val();
                $.ajax({
                    url: "<?php echo route('pengurus-cek-tagihan-listrik') ?>",
                    method: 'POST',
                    data: {_token:token,nomor:nomor},
                    success: function(data) {
                      // alert(data.code);
                      console.log(data);
                      $('.modal').modal('hide');
                      if (data.code == '200') {
                        $("#message").html("");
                        $("#form").html("");
                        $("#form").hide();
                        $("#respon").show();
                        $("#message").html(data.messages);

                      }else {
                        $("#message").html("");
                        $("#errors").show();
                        $("#errors").html(data.messages);
                        setTimeout(function () {
                         	$('#errors').modal('hide');
                          $('#errors').html('');
                        }, 5000);
                      }
                    }
                });
            });
          </script>
        </form>
        </div>
        <div class="col-sm-8 col-md-6 col-xs-12">
          <div class="row">
            <div class="col-xs-12">
              <table width="100%" class="table table-striped custom-table">
                <tr>
                  <td>
                    <h4 class="page-title text-center"> MENU LAIN</h4>
                  </div>
                    @foreach($menus as $menu)
                    <div class="col-sm-6 col-md-6 col-xs-6">
                        <a href="{{url('anggota/'.$menu->route)}}"><button type="button" name="button"  class="btn btn-success btn-sm m-t-10 form-control">{{$menu->menu}}</button></a>
                    </div>
                    @endforeach
                  </div>
                  </td>
                </tr>
              </table>
            </div>
          </div>
          <!-- MODAL LOADING -->
          <style>
          .bd-example-modal-lg .modal-dialog{
            display: table;
            position: relative;
            margin: 0 auto;
            text-align:center;
            top: calc(50% - 24px);
          }
          .bd-example-modal-lg .modal-dialog .modal-content{
            background-color: #ffffff;
            border: none;
          }
          </style>
          <div class="modal fade bd-example-modal-lg" data-backdrop="static" data-keyboard="false" tabindex="-1">
              <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                      <br><span class="fa fa-spinner fa-spin fa-5x"></span><br><br>
                      Mohon Menunggu...<br>
                  </div>

              </div>
          </div>
          <!-- MODAL -->
    </div>
</div>
@endsection
