@extends('layouts.umum.pages')
@section('content')
<script type="text/javascript" src="https://formden.com/static/cdn/formden.js"></script>
<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />
<link rel="stylesheet" href="https://formden.com/static/cdn/font-awesome/4.4.0/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="{{url('css/bootstrap-datetimepicker.min.css')}}">
<div class="page-wrapper">
    <div class="content container-fluid">
<section id="content">
  <div class="container">
    <div class="row">
      <div class="span12">
        <h4><strong>Form Pendaftaran KS212</strong></h4>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{url('/daftar')}}" method="post" enctype="multipart/form-data">
          @csrf
              <div class="row">
                <div class="form-group span4">
                  <label class="control-label">No Pendaftaran</label>
                  <?php $noanggota = App\User::orderBy('no_anggota','DESC')->first();
                  $nomor = (int)$noanggota->no_anggota+1;
                  $sequence = substr($nomor,-6);
                  $anggota = date('Ym').$sequence; ?>
<?php $anggota = date('YmdHi'); ?>
                  <input class="form-control" type="text" name="no_anggota" value="{{$anggota}}" disabled>
              </div>
              <div class="form-group span4">
                <label class="control-label">Nama Lengkap<span class="text-danger">*</span></label>
                <input class="form-control" type="text" name="name" required>
            </div>
              <div class="form-group span4">
                <label class="control-label">No. KTP<span class="text-danger">*</span></label>
                <input class="form-control" type="text" name="nik" required>
              </div>
              <div class="form-group span4">
                <label class="control-label">Nomor Telpon <span class="text-danger">*</span></label>
                <input class="form-control" type="text" name="telp" required>
              </div>
              <div class="form-group span4">
                <label class="control-label">Email <span class="text-danger">*</span></label>
                <input class="form-control" type="email" name="email" required>
              </div>
                <div class="form-group span4">
                  <label class="control-label">Tempat Lahir</label>
                  <input class="form-control" type="text" name="tpt_lahir">
                </div>
                <div class="form-group span4">
                  <label class="control-label">Tanggal Lahir</label>
                  <div class="cal-icon"><input class="form-control floating datetimepicker" id="date" type="text" name="tgl_lahir" placeholder="DD-MM-TTTT"></div>
                </div>

                <div class="form-group span4">
                  <label class="control-label">NPWP</label>
                  <input class="form-control" type="text" name="npwp">
                </div>

                <div class="form-group span4">
                  <label class="control-label">Pendidikan Terakhir </label>
                  <input type="text" class="form-control" name="pendidikan">
                </div>
                <div class="form-group span4">
                  <label class="control-label">Status Dalam Keluarga</label>
                  <input type="text" class="form-control" name="statuskeluarga">
                </div>
                <div class="form-group span4">
                  <label class="control-label">Nama Ibu</label>
                  <input type="text" class="form-control" name="nama_ibu">
                </div>
                <div class="form-group span4">
                  <label class="control-label">Nama Ayah</label>
                  <input type="text" class="form-control" name="nama_ayah">
                </div>
                <div class="form-group span4">
                  <label class="control-label">Alamat Anggota </label>
                  <input class="form-control" type="text" name="alamat">
                </div>
                <div class="form-group span4">
                  <label class="control-label">Kelurahan </label>
                  <input class="form-control" type="text" name="kelurahan">
                </div>
                <div class="form-group span4">
                  <label class="control-label">Kecamatan </label>
                  <input class="form-control" type="text" name="kecamatan">
                </div>
                <div class="form-group span4">
                  <label class="control-label">Jenis Kelamin </label>
                  <select class="select form-control" name="jenkel">
                    <option value="">Pilih Jenis Kelamin</option>
                    <option value="Laki-laki">LAKI-LAKI</option>
                    <option value="Perempuan">PEREMPUAN</option>
                  </select>
                </div>
                <div class="form-group span4">
                  <label class="control-label">Agama </label>
                  <select class="select form-control" name="agama">
                    <option value="">Pilih Agama</option>
                    <option value="islam">Islam</option>
                  </select>
                </div>
                <div class="form-group span4">
                  <label class="control-label">Komunitas </label>
                  <select class="select form-control" name="komunitas">
                    <option value="">Pilih Komunitas</option>
                    <?php $komunitas = App\Komunitas::where('aktif',1)->get(); ?>
                    @foreach($komunitas as $komu)
                    <option value="{{$komu->id}}">{{$komu->name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group span4">
                  <label class="control-label">Propinsi </label>
                  <select class="select form-control" name="propinsi">
                    <option value="">Pilih Komunitas</option>
                    <?php $propinsi = App\Propinsi::where('aktif',1)->get(); ?>
                    @foreach($propinsi as $pro)
                    <option value="{{$pro->id}}">{{$pro->name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group span4">
                  <label class="control-label">Pendapatan</label>
                  <select class="select form-control" name="pendapatan">
                    <option value="">Pilih Pendapatan</option>
                    <?php $pendapatan = App\Pendapatan::where('aktif',1)->get(); ?>
                    @foreach($pendapatan as $pend)
                    <option value="{{$pend->id}}">{{$pend->name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group span4">
                  <label class="control-label">Pekerjaan </label>
                  <select class="select form-control" name="pekerjaan">
                    <option value="">Pilih Pekerjaan</option>
                    <?php $pekerjaan = App\Pekerjaan::where('aktif',1)->get(); ?>
                    @foreach($pekerjaan as $pek)
                    <option value="{{$pek->id}}">{{$pek->name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group span4">
                  <label class="control-label">Simpanan Pokok </label>
                  <input class="form-control" type="text" name="pokok" value="Rp. 130.000,-" disabled>
                </div>
                <div class="form-group span4">
                  <label class="control-label">Simpanan Wajib </label>
                  <input class="form-control" type="text" name="wajib" value="Rp. 120.000,-" disabled>
                </div>
                <div class="form-group span4">
                  <label class="control-label">Simpanan Sukarela </label>
                  <input class="form-control" type="number" name="sukarela" value="" required>
                </div>
                <div class="form-group span4">
                  <label class="control-label">Investasi </label>
                  <input class="form-control" type="number" name="investasi" value="" required>
                </div>
                <div class="form-group span4">
                  <label class="control-label">Wakaf </label>
                  <input class="form-control" type="number" name="wakaf" value="" required>
                </div>
                <div class="form-group span4">
                  <label class="control-label">Infaq / Sodaqoh </label>
                  <input class="form-control" type="number" name="infaq" value="" required>
                </div>
                <div class="form-group span4">
                  <label class="control-label">Foto Dokumen KTP<span class="text-danger">*</span></label>
                  <input class="" type="file" name="fotoktp" required>
                </div>
                <div class="form-group span4">
                  <label class="control-label">Foto Diri<span class="text-danger">*</span></label>
                  <input class="" type="file" name="fotodiri" required>
                </div>
              </div>
            <!-- <hr>
          <center><h3>SIMPANAN</h3></center>
          <hr>
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label class="control-label">Simpanan Pokok<span class="text-danger">*</span></label>
                  <input type="text" class="form-control" name="pokok" value="" required>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label class="control-label">Simpanan Wajib<span class="text-danger">*</span></label>
                  <input type="text" class="form-control" name="wajib" required>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label class="control-label">Simpanan Sukarela<span class="text-danger">*</span></label>
                  <input type="text" class="form-control" name="sukarela" required>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label class="control-label">Simpanan Infaq<span class="text-danger">*</span></label>
                  <input type="text" class="form-control" name="infaq" required>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label class="control-label">Simpanan Wakaf<span class="text-danger">*</span></label>
                  <input type="text" class="form-control" name="wakaf" required>
                </div>
              </div>
            </div> -->
            <div class="row">
            <div class="span4 text-center">
              <!-- <button class="btn btn-green form-control" type="submit">DAFTAR</button> -->
            </div>
            <div class="span4 text-center">
              <button class="btn btn-green form-control" type="submit">DAFTAR</button>
            </div>
            <div class="span4 text-center">
              <!-- <button class="btn btn-green form-control" type="submit">DAFTAR</button> -->
            </div>
          </div>
          </div>
          <input type="hidden" name="no_anggota" value="{{$anggota}}">
        </form>
      </div>
    </div>
  </div>
</section>
</div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
<script>
	$(document).ready(function(){
		var date_input=$('input[name="date"]'); //our date input has the name "date"
		var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
		date_input.datepicker({
			format: 'mm/dd/yyyy',
			container: container,
			todayHighlight: true,
			autoclose: true,
		})
	})
</script>
@endsection
