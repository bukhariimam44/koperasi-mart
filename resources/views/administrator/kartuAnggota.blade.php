@extends('layouts.admin.app')
@section('content')
            <div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
						<div class="col-sm-12 col-xs-12">
							<h4 class="page-title">Kartu Anggota</h4>
						</div>
					</div>
					<div class="row filter-row">
            <form class="m-b-30" action="{{url('/administrator/kartu-anggota')}}" method="post">
              @csrf
              <input type="hidden" name="action" value="cari">
              <div class="col-sm-4 col-xs-6">
  							<div class="form-group form-focus">
  								<label class="control-label">No Anggota</label>
  								<input type="number" class="form-control floating" name="no_anggota" value="{{$anggota}}"/>
  							</div>
  						</div>
              <div class="col-sm-4 col-xs-6">
  							<div class="form-group form-focus">
  								<label class="control-label">Nama Anggota</label>
  								<input type="text" class="form-control floating" name="name" value="{{$nama}}"/>
  							</div>
  						</div>
  						<div class="col-sm-4 col-xs-12">
  							<button type="submit" class="btn btn-success btn-block"> TAMPILKAN </button>
  						</div>
            </form>
          </div>
					<div class="row staff-grid-row">
            <style>
              #kartuname {
                position: absolute;
                top:87px;
                color: black;
                font-size: 6px;
                bottom: 100px;
                margin-right: 33px;
                right: 35px;
                overflow: hidden;
              }
            </style>
            @foreach($users as $user)
						<div class="col-md-4 col-sm-4 col-xs-6 col-lg-3">
							<div class="profile-widget">
								<div class="profile-imges">
									<a href="#"  data-toggle="modal" data-target="#lihat">@if($user->fotodiri !==null)<img src="{{asset('foto/'.$user->fotodiri)}}" width="20%" style="margin-bottom:-46%;margin-left:-23%;margin-right:50%"><center><img src="{{asset('kartu/new.png')}}" width="100%" id="img1" style="margin-top:-17px;"/></center>@else <img src="{{asset('kartu/new.png')}}" width="100%" id="img1" style="margin-top:1px;"/>@endif</a>
								</div>
								<h4 class="user-name m-t-10 m-b-0 text-ellipsis">{{$user->no_anggota}}</h4>
								<h5 class="user-name m-t-10 m-b-0 text-ellipsis">{{$user->name}}</h5>
								<div class="small text-muted"></div>
								<a href="{{url('/administrator/Download-kartu/'.$user->sequence)}}" class="btn btn-default btn-sm m-t-10">Download</a>
								<a href="#" data-toggle="modal" data-target="#upload{{$user->id}}" class="btn btn-default btn-sm m-t-10">Upload</a>
							</div>
						</div>
            @endforeach

					</div>
                </div>
                <?php $editusers = App\User::get(); ?>
                @foreach($users as $edituser)
			<div id="lihat{{$edituser->id}}" class="modal custom-modal fade" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content modal-md">
						<div class="modal-header">
							<h4 class="modal-title">KARTU ANGGOTA</h4>
						</div>
						<div class="modal-body card-box">
							<p>@if($edituser->fotodiri==null)<img src="{{asset('kartu/new.png')}}" width="100%"/>@else <img src="{{asset('foto/'.$edituser->fotodiri)}}" width="20%" style="margin-bottom:-290px;margin-left:17px;margin-right:50px"><img src="{{asset('kartu/new.png')}}" width="100%" id="img1" style="margin-top:-20px"/>@endif</p>
							<div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
								<!-- <button type="submit" class="btn btn-danger">Delete</button> -->
							</div>
						</div>
					</div>
				</div>
			</div>
      <div id="upload{{$edituser->id}}" class="modal custom-modal fade" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content modal-md">
						<div class="modal-header">
							<h4 class="modal-title">UPLOAD KARTU ANGGOTA</h4>
						</div>
						<div class="modal-body card-box">
              <form class="" action="{{url('/administrator/kartu-anggota')}}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="ids" value="{{$edituser->id}}">
                <input type="hidden" name="action" value="upload">
                <p>No Anggota : {{$edituser->no_anggota}}</p>
  							<p> <input type="file" name="fotodiri"> </p>
  							<div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
  								<button type="submit" class="btn btn-success">Upload</button>
  							</div>
              </form>
						</div>
					</div>
				</div>
			</div>
      @endforeach
    </div>
		<div class="sidebar-overlay" data-reff="#sidebar"></div>
@endsection
