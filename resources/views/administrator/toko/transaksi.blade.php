@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <h4 class="page-title">Transaksi</h4>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-xs-12">
          <div class="row">
          @foreach($menus as $menu)
            <div class="col-md-6 col-xs-6">
                <a href="{{url('administrator/'.$menu->route)}}"><button type="button" name="button"  class="btn btn-success btn-sm m-t-10 form-control">{{$menu->menu}}</button></a>
            </div>
          @endforeach
        </div>
      </div>
      <div class="col-md-6 col-xs-12">
        <div class="row">
          <!-- ISI KOLOM -->
      </div>
    </div>
      </div>
    </div>
</div>
@endsection
