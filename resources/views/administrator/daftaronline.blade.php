@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">


<div class="row">
	<div class="col-xs-6">
		<h4 class="page-title">Data Pendaftaran Online</h4>
	</div>
  <div class="col-xs-6">
    <!-- <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_golongan_pegawai"><i class="fa fa-plus"></i> Tambah Propinsi</a> -->
  </div>
</div>

<div class="row filter-row">
  <form class="" action="{{url('/administrator/daftar-online')}}" method="post">
    @csrf

	<div class="col-sm-6 col-xs-6">
		<div class="form-group form-focus">
			<label class="control-label">Nomor Pendaftaran</label>
			<input type="text" name="nomor" value="" class="form-control floating">
		</div>
	</div>
	<div class="col-sm-6 col-xs-6">
		<!-- <a href="#" class="btn btn-success btn-block"> Search </a> -->
    <input type="submit" class="btn btn-success btn-block" name="btn" value="Search">
	</div>
</form>
</div>

<div class="row">
	<div class="col-sx-6 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped custom-table datatable">
			<!-- <a href="{{url('#')}}" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_golongan_pegawai"><i class="fa fa-plus"></i>Tambah Golongan</a> -->

				<thead>
					<tr>
						<th>No.</th>
						<th>Nomor Anggota</th>
						<th>Tanggal Daftar</th>
            <th>Nama Lengkap</th>
            <th>Total Transfer</th>
            <th>Status</th>
            <th>Action</th>
					</tr>
				</thead>
				<tbody>
          <?php $no=1; $daftars = App\Daftar::where('aktif',1)->get(); ?>
          @foreach($daftars as $daft)
					<tr class="holiday-completed">
						<td>{{$no++}}.</td>
            <td>{{$daft->no_anggota}}</td>
            <td>{{$daft->tgl_daftar}}</td>
						<td>{{$daft->name}}</td>
            <td>{{number_format($daft->pokok+$daft->wajib+$daft->sukarela+$daft->investasi+$daft->wakaf+$daft->infaq)}}</td>
						<td>{{$daft->status_id->name}}</td>
            <td style="min-width:150px;">
							<a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#detail{{$daft->id}}">Detail</a>
						</td>
					</tr>
          @endforeach
				</tbody>

			</table>
		</div>
	</div>
</div>

<!-- <div id="add_golongan_pegawai" class="modal custom-modal fade" role="dialog">
  <div class="modal-dialog">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-content modal-md">
      <div class="modal-header">
        <h4 class="modal-title">Form Propinsi</h4>
      </div>
      <div class="modal-body">
        <form action="{{url('/administrator/data-propinsi')}}" method="post">
            @csrf
          <div class="form-group">
            <label>Nama Propinsi <span class="text-danger">*</span></label>
            <input class="input-sm form-control" required="" type="text" name="propinsi">
          </div>
          <div class="m-t-20 text-center">
            <input class="btn btn-primary" type="submit" value="SIMPAN"/>
          </div>
        </form>
      </div>
    </div>
  </div>
</div> -->
<!-- ///////////////? -->
<!-- <div id="add_golongan_anggota" class="modal custom-modal fade" role="dialog">
  <div class="modal-dialog">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-content modal-md">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Golongan Anggota</h4>
      </div>
      <div class="modal-body">
        <form action="{{url('/admin/tambah-golongan-anggota')}}" method="post">
          @csrf
          <div class="form-group">
            <label>Nama Golongan <span class="text-danger">*</span></label>
            <input class="input-sm form-control" required="" type="text" name="golongan">
          </div>
          <div class="form-group">
            <label>Nama Pangkat <span class="text-danger">*</span></label>
            <input class="input-sm form-control" required="" type="text" name="pangkat">
          </div>
          <div class="m-t-20 text-center">
            <input class="btn btn-primary" type="submit" value="SIMPAN GOLONGAN"/>
          </div>
        </form>
      </div>
    </div>
  </div>
</div> -->
<?php $detail = App\Daftar::where('aktif',1)->get(); ?>
@foreach($detail as $det)
<div id="detail{{$det->id}}" class="modal custom-modal fade" role="dialog">
  <div class="modal-dialog">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-content modal-md">
      <div class="modal-header">
        <h4 class="modal-title">Detail Pendaftaran</h4>
      </div>
      <div class="modal-body">
        <form action="{{url('/administrator/daftar-online/'.$det->id)}}" method="post">
          <input type="hidden" name="action" value="proses">
          @csrf
          <table class="table">
            <tr>
              <td>Nomor Pendaftaran</td>
              <td>: {{$det->no_anggota}}</td>
            </tr>
            <tr>
              <td>Nama Lengkap</td>
              <td>: {{$det->name}}</td>
            </tr>
            <tr>
              <td>No KTP</td>
              <td>: {{$det->nik}}</td>
            </tr>
            <tr>
              <td>Email</td>
              <td>: {{$det->email}}</td>
            </tr>
            <tr>
              <td>No HP</td>
              <td>: {{$det->telp}}</td>
            </tr>
            <tr>
              <td>Simpanan Pokok</td>
              <td>: Rp. {{number_format($det->pokok)}},-</td>
            </tr>
            <tr>
              <td>Simpanan Wajib</td>
              <td>: Rp. {{number_format($det->wajib)}},-</td>
            </tr>
            <tr>
              <td>Simpanan Sukarela</td>
              <td>: Rp. {{number_format($det->sukarela)}},-</td>
            </tr>
            <tr>
              <td>Investasi</td>
              <td>: Rp. {{number_format($det->investasi)}},-</td>
            </tr>
            <tr>
              <td>Wakaf</td>
              <td>: Rp. {{number_format($det->wakaf)}},-</td>
            </tr>
            <tr>
              <td>Infaq / Sodaqoh</td>
              <td>: Rp. {{number_format($det->infaq)}},-</td>
            </tr>
            <tr>
              <td>Nomor Invoic</td>
              <td>: {{$det->invoic}}</td>
            </tr>
            <tr>
              <td><strong>Total Simpanan</strong></td>
              <td>: <strong>Rp. {{number_format($det->pokok+$det->wajib+$det->sukarela+$det->investasi+$det->wakaf+$det->infaq)}},-</strong></td>
            </tr>
          </table>
          <div class="m-t-20 text-center">
            <button class="btn btn-success">PROSES</button> <button class="btn btn-danger" onclick="event.preventDefault();
                          document.getElementById('cancel-daftar').submit();">CANCEL</button>
          </div>
        </form>
        <form class="" action="{{url('/administrator/daftar-online/'.$det->id)}}" method="post" id="cancel-daftar">
          @csrf
          <input type="hidden" name="action" value="cancel">
        </form>
      </div>
    </div>
  </div>
</div>
@endforeach


<div id="hapus_golongan" class="modal custom-modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content modal-md">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Data</h4>
      </div>
      <form action="{{url('/admin/hapus-golongan-pegawai/')}}" method="post" id="hapus_golongan">
        @csrf
        <div class="modal-body card-box">
          <p>Apakah yakin ingin di Hapus :  ???</p>
          <div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
            <button type="submit" class="btn btn-danger">Delete</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

  </div>
<div class="sidebar-overlay" data-reff="#sidebar"></div>
</div>
</div>
@endsection
