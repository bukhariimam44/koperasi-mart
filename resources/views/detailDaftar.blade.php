@extends('layouts.umum.pages')
@section('content')
<section id="content">
  <div class="container">
    <div class="row">
      <div class="span12">
        Terim kasih atas kepercayaan anda untuk bergabung bersama KSSD
    Penyetoran Pertama Anggota Baru terdiri dari </br>
    1. Simpanan Pokok Rp. 130.000,-</br>
    2. Simpanan Wajib Rp. 120.000,-</br>
    3. Simpanan Sukarela Rp. {{number_format($sukarela)}},-</br>
    4. Investasi Rp. {{ number_format($investasi) }},-</br>
    5. Wakaf Rp.  {{ number_format($wakaf) }},-</br>
    6. Infaq/Sodaqoh Rp. {{ number_format($infaq) }},-</br></br>

    Silahkan transfer ke Bank Muamalat </br>
    No. Rekening (147) 3330  212  112</br>
    Total Transfer : Rp. <?php echo number_format($sukarela+$investasi+$wakaf+$infaq+250000); ?>,-</br>
    Dengan Berita/Keterangan : {{$random}}</br></br>

    Bukti transfer disampaikan ke</br>
    Call Center :</br>WA 087 808 212 112
      </div>
    </div>
  </div>
</section>
@endsection
