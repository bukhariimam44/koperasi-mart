<!-- start header -->
<header>
  <div class="container ">
    <!-- hidden top area toggle link -->
    <div id="header-hidden-link">
      <!-- <a href="#" class="toggle-link" title="Click me you'll get a surprise" data-target=".hidden-top"><i></i>Open</a> -->
    </div>
    <!-- end toggle link -->
    <div class="row nomargin">
      <div class="span12">
        <div class="headnav">
          <!-- <ul>
            <li><a href="#mySignup" data-toggle="modal"><i class="icon-user"></i> Sign up</a></li>
            <li><a href="#mySignin" data-toggle="modal">Sign in</a></li>
          </ul> -->
        </div>
        <!-- Signup Modal -->
        <!-- <div id="mySignup" class="modal styled hide fade" tabindex="-1" role="dialog" aria-labelledby="mySignupModalLabel" aria-hidden="true">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 id="mySignupModalLabel">Create an <strong>account</strong></h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal">
              <div class="control-group">
                <label class="control-label" for="inputEmail">Email</label>
                <div class="controls">
                  <input type="text" id="inputEmail" placeholder="Email">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="inputSignupPassword">Password</label>
                <div class="controls">
                  <input type="password" id="inputSignupPassword" placeholder="Password">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="inputSignupPassword2">Confirm Password</label>
                <div class="controls">
                  <input type="password" id="inputSignupPassword2" placeholder="Password">
                </div>
              </div>
              <div class="control-group">
                <div class="controls">
                  <button type="submit" class="btn">Sign up</button>
                </div>
                <p class="aligncenter margintop20">
                  Already have an account? <a href="#mySignin" data-dismiss="modal" aria-hidden="true" data-toggle="modal">Sign in</a>
                </p>
              </div>
            </form>
          </div>
        </div> -->
        <!-- end signup modal -->
        <!-- Sign in Modal -->
        <!-- <div id="mySignin" class="modal styled hide fade" tabindex="-1" role="dialog" aria-labelledby="mySigninModalLabel" aria-hidden="true">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 id="mySigninModalLabel">Login <strong>KSSD-CILEGON</strong></h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal">
              <div class="control-group">
                <label class="control-label" for="inputText">No Anggota</label>
                <div class="controls">
                  <input type="text" id="inputText" placeholder="No Anggota"name="sequence">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="inputSigninPassword">Password</label>
                <div class="controls">
                  <input type="password" id="inputSigninPassword" placeholder="Password" name="password">
                </div>
              </div>
              <div class="control-group">
                <div class="controls">
                  <button type="submit" class="btn">Sign in</button>
                </div>
                <p class="aligncenter margintop20">
                  Forgot password? <a href="#myReset" data-dismiss="modal" aria-hidden="true" data-toggle="modal">Reset</a>
                </p>
              </div>
            </form>
          </div>
        </div> -->
        <!-- end signin modal -->
        <!-- Reset Modal -->
        <div id="myReset" class="modal styled hide fade" tabindex="-1" role="dialog" aria-labelledby="myResetModalLabel" aria-hidden="true">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 id="myResetModalLabel">Reset your <strong>password</strong></h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal">
              <div class="control-group">
                <label class="control-label" for="inputResetEmail">Email</label>
                <div class="controls">
                  <input type="text" id="inputResetEmail" placeholder="Email">
                </div>
              </div>
              <div class="control-group">
                <div class="controls">
                  <button type="submit" class="btn">Reset password</button>
                </div>
                <p class="aligncenter margintop20">
                  We will send instructions on how to reset your password to your inbox
                </p>
              </div>
            </form>
          </div>
        </div>
        <!-- end reset modal -->
      </div>
    </div>
    <div class="row">
      <div class="span4">
        <div class="logo">
          <a href="{{url('/')}}"><img src="{{url('images/kseu.jpeg')}}" style="max-width:130px;" alt="" /></a>
        </div>
      </div>
      <div class="span8">
        <div class="navbar navbar-static-top">
          <div class="navigation">
            <nav>
              <ul class="nav topnav">
                <li class="active">
                  <a href="{{url('/')}}"><strong>Home</strong></a>
                </li>
                <!-- <li class="dropdown">
                  <a href="#">Blog <i class="icon-angle-down"></i></a>
                  <ul class="dropdown-menu">
                    <li><a href="blog-left-sidebar.html">Blog left sidebar</a></li>
                    <li><a href="blog-right-sidebar.html">Blog right sidebar</a></li>
                    <li><a href="post-left-sidebar.html">Post left sidebar</a></li>
                    <li><a href="post-right-sidebar.html">Post right sidebar</a></li>
                  </ul>
                </li> -->
                <li><a href="{{url('/profil')}}"><strong>Profil</strong> </a></li>
                <li><a href="{{url('/struktur-organisasi')}}"><strong>Organisasi</strong> </a></li>
                <li><a href="{{url('/daftar')}}"><strong>Pendaftaran</strong> </a></li>
                <li><a href="{{url('/kontak')}}"><strong>Kontak</strong> </a></li>
                <li class="active"><a href="{{url('/home')}}"><strong>Login</strong></a></li>
              </ul>
            </nav>
          </div>
          <!-- end navigation -->
        </div>
      </div>
    </div>
  </div>
</header>
<!-- end header -->
