<div class="sidebar" id="sidebar">
    <div class="sidebar-inner slimscroll">
<div id="sidebar-menu" class="sidebar-menu">
<ul>
  <li class="{{setActive(['anggota-home'])}}">
    <a href="{{route('anggota-home')}}">DASHBOARD ANGGOTA</a>
  </li>
  <!-- <li class="{{setActive(['anggota-belanja'])}} submenu">
    <a href="#"><span>TRANSAKSI</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{url('/anggota/belanja')}}">&nbsp;&nbsp;&nbsp;Belanja Barang</a></li>
      <li><a href="{{url('/anggota/pulsa')}}">&nbsp;&nbsp;&nbsp;Pulsa HP</a></li>
      <li><a href="{{url('/anggota/paket-data-internet')}}">&nbsp;&nbsp;&nbsp;Paket Data Internet</a></li>
      <li><a href="{{url('/anggota/voucher-listrik')}}">&nbsp;&nbsp;&nbsp;Voucher Listrik</a></li>
      <li><a href="{{url('/anggota/paket-sms')}}">&nbsp;&nbsp;&nbsp;Paket SMS/Telp</a></li>
      <li><a href="{{url('/anggota/transfer-pulsa')}}">&nbsp;&nbsp;&nbsp;Transfer Pulsa</a></li>
      <li><a href="{{url('/anggota/saldo-ojek')}}">&nbsp;&nbsp;&nbsp;Saldo Ojek</a></li>
      <li><a href="{{route('anggota-cek-tagihan-listrik')}}">&nbsp;&nbsp;&nbsp;Tagihan Listrik</a></li>
      <li><a href="{{route('anggota-data-transaksi-ppob')}}">&nbsp;&nbsp;&nbsp;Laporan PPOB</a></li>
      <li><a href="{{route('anggota-data-transaksi-belanja')}}">&nbsp;&nbsp;&nbsp;Laporan Belanja</a></li>
    </ul>
  </li> -->
  <li class="{{setActive(['anggota-profil'])}}">
    <a href="{{route('anggota-profil')}}">PROFIL</a>
  </li>
  <li class="{{setActive(['anggota-kartu'])}}">
    <a href="{{route('anggota-kartu')}}">KARTU ANGGOTA</a>
  </li>
  <li class="{{setActive(['anggota-simpanan'])}}">
    <a href="{{route('anggota-simpanan')}}">SIMPANAN</a>
  </li>
 <li class="{{setActive(['anggota-statistik','anggota-statistik-penjualan','anggota-statistik-jenis-kelamin','anggota-statistik-kelurahan','anggota-statistik-kecamatan','anggota-statistik-kabupaten'])}} submenu">
    <a href="#"><span>STATISTIK</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a class="{{setActive(['anggota-statistik'])}}" href="{{route('anggota-statistik')}}">&nbsp;&nbsp;&nbsp;Statistik Simpanan</a></li>
      <li><a class="{{setActive(['anggota-statistik-penjualan'])}}" href="{{route('anggota-statistik-penjualan')}}">&nbsp;&nbsp;&nbsp;Statistik Penjualan</a></li>
      <li><a class="{{setActive(['anggota-statistik-jenis-kelamin'])}}" href="{{route('anggota-statistik-jenis-kelamin')}}">&nbsp;&nbsp;&nbsp;Statistik Jenis Kelamin</a></li>
      <li><a class="{{setActive(['anggota-statistik-kelurahan'])}}" href="{{route('anggota-statistik-kelurahan')}}">&nbsp;&nbsp;&nbsp;Statistik Kelurahan</a></li>
      <li><a class="{{setActive(['anggota-statistik-kecamatan'])}}" href="{{route('anggota-statistik-kecamatan')}}">&nbsp;&nbsp;&nbsp;Statistik Kecamatan</a></li>
      <li><a class="{{setActive(['anggota-statistik-kabupaten'])}}" href="{{route('anggota-statistik-kabupaten')}}">&nbsp;&nbsp;&nbsp;Statistik Kabupaten</a></li>
    </ul>
  </li>
  <li class="{{setActive(['anggota-akumulasi-belanja'])}}">
    <a href="{{route('anggota-akumulasi-belanja')}}">AKUMULASI BELANJA</a>
  </li>
  <!-- <li class="active submenu">
    <a href="#"><span>DEPOSIT</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{route('topup-deposit-anggota')}}">&nbsp;&nbsp;&nbsp;Topup Saldo</a></li>
      <li><a href="{{route('data.topup')}}">&nbsp;&nbsp;&nbsp;Laporan Deposit</a></li>
    </ul>
  </li> -->
  <li class="{{setActive(['anggota-buku-saldo-simpanan'])}}">
    <a href="{{route('anggota-buku-saldo-simpanan')}}">BUKU SALDO SIMPANAN</a>
  </li>
  <!-- <li class="active submenu">
    <a href="#"><span>BUKU SALDO</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{url('/anggota/buku-saldo-simpanan')}}">&nbsp;&nbsp;&nbsp;Buku Saldo Simpanan</a></li>
      <li><a href="{{route('anggota-buku-saldo-transaksi')}}">&nbsp;&nbsp;&nbsp;Buku Saldo Transaksi</a></li>
    </ul>
  </li> -->
  <li class="{{setActive(['anggota-ganti-password'])}}">
    <a href="{{route('anggota-ganti-password')}}">GANTI PASSWORD</a>
  </li>
  <li>
    <a href="{{ route('logout') }}" onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">KELUAR</a>
  </li>
</ul>
</div>
    </div>
</div>
