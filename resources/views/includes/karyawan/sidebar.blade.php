<div class="sidebar" id="sidebar">
    <div class="sidebar-inner slimscroll">
<div id="sidebar-menu" class="sidebar-menu">
<ul>
  <li class="{{setActive(['/karyawan/home'])}}">
    <a href="{{route('/karyawan/home')}}">DASHBOARD KARYAWAN</a>
  </li>
  <li class="{{setActive(['/karyawan/data-anggota'])}}">
    <a href="{{route('/karyawan/data-anggota')}}">DATA ANGGOTA</a>
  </li>
  <li class="{{setActive(['/karyawan/akumulasi-belanja'])}} submenu">
    <a href="#"><span>AKUMULASI BELANJA</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a class="{{setActive(['/karyawan/akumulasi-belanja'])}}" href="{{route('/karyawan/akumulasi-belanja')}}">&nbsp;&nbsp;&nbsp;Laporan Akumulasi</a></li>
      <!-- <li><a href="{{url('/karyawan/saldo-akumulasi-belanja')}}">&nbsp;&nbsp;&nbsp;Saldo Akumulasi</a></li> -->
    </ul>
  </li>
  <li class="{{setActive(['/karyawan/profil'])}}">
    <a href="{{route('/karyawan/profil')}}">PROFIL</a>
  </li>
  <li class="{{setActive(['/karyawan/ganti-password'])}}">
    <a href="{{route('/karyawan/ganti-password')}}">GANTI PASSWORD</a>
  </li>
  <li>
    <a href="{{ route('logout') }}" onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">KELUAR</a>
  </li>
</ul>
</div>
    </div>
</div>
