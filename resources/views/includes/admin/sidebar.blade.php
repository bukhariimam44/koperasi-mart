<div class="sidebar" id="sidebar">
    <div class="sidebar-inner slimscroll">
<div id="sidebar-menu" class="sidebar-menu">
<ul>
  <li class="{{setActive(['home','home-admin'])}}">
    <a href="{{route('home-admin')}}">DASHBOARD ADMIN</a>
  </li>
  <li class="{{setActive(['administrator-data-pendidikan','administrator-data-komunitas','administrator-data-kelurahan','administrator-data-kecamatan','administrator-data-kabupaten','administrator-data-propinsi','administrator-data-pendapatan','administrator-data-pekerjaan','administrator-data-jenis-simpanan','administrator-data-rekening'])}} submenu">
    <a href="#"><span>DATA MASTER</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a class="{{setActive(['administrator-data-pendidikan'])}}" href="{{route('administrator-data-pendidikan')}}">&nbsp;&nbsp;&nbsp;Data Pendidikan</a></li>
      <li><a class="{{setActive(['administrator-data-komunitas'])}}" href="{{route('administrator-data-komunitas')}}">&nbsp;&nbsp;&nbsp;Data Komunitas</a></li>
      <li><a class="{{setActive(['administrator-data-kelurahan'])}}" href="{{route('administrator-data-kelurahan')}}">&nbsp;&nbsp;&nbsp;Data Kelurahan</a></li>
      <li><a class="{{setActive(['administrator-data-kecamatan'])}}" href="{{route('administrator-data-kecamatan')}}">&nbsp;&nbsp;&nbsp;Data Kecamatan</a></li>
      <li><a class="{{setActive(['administrator-data-kabupaten'])}}" href="{{route('administrator-data-kabupaten')}}">&nbsp;&nbsp;&nbsp;Data Kabupaten</a></li>
      <li><a class="{{setActive(['administrator-data-propinsi'])}}" href="{{route('administrator-data-propinsi')}}">&nbsp;&nbsp;&nbsp;Data Propinsi</a></li>
      <li><a class="{{setActive(['administrator-data-pendapatan'])}}" href="{{route('administrator-data-pendapatan')}}">&nbsp;&nbsp;&nbsp;Data Pendapatan</a></li>
      <li><a class="{{setActive(['administrator-data-pekerjaan'])}}" href="{{route('administrator-data-pekerjaan')}}">&nbsp;&nbsp;&nbsp;Data Pekerjaan</a></li>
      <li><a class="{{setActive(['administrator-data-jenis-simpanan'])}}" href="{{route('administrator-data-jenis-simpanan')}}">&nbsp;&nbsp;&nbsp;Jenis Simpanan</a></li>
      <li><a class="{{setActive(['administrator-data-rekening'])}}" href="{{route('administrator-data-rekening')}}">&nbsp;&nbsp;&nbsp;Data Rekening</a></li>
    </ul>
  </li>
  <li class="{{setActive(['administrator-data-anggota','administrator-data-group','administrator-data-karyawan'])}} submenu">
    <a href="#"><span>DATA PENGGUNA</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a class="{{setActive(['administrator-data-anggota'])}}" href="{{route('administrator-data-anggota')}}">&nbsp;&nbsp;&nbsp;Data Anggota</a></li>
      <li><a class="{{setActive(['administrator-data-group'])}}" href="{{route('administrator-data-group')}}">&nbsp;&nbsp;&nbsp;Data Group</a></li>
      <li><a class="{{setActive(['administrator-data-karyawan'])}}" href="{{route('administrator-data-karyawan')}}">&nbsp;&nbsp;&nbsp;Data Karyawan</a></li>
      <!-- <li><a href="{{url('/administrator/simpanan-jatuh-tempo')}}">&nbsp;&nbsp;&nbsp;Data Jatuh Tempo</a></li> -->
    </ul>
  </li>
  <li class="{{setActive(['administrator-data-simpanan'])}}">
    <a href="{{route('administrator-data-simpanan')}}">SIMPANAN ANGGOTA</a>
  </li>
  <li class="{{setActive(['administrator-status-simpanan-wajib'])}}">
    <a href="{{route('administrator-status-simpanan-wajib')}}">STATUS SIMPANAN WAJIB</a>
  </li>
  <li class="{{setActive(['administrator-buku-saldo'])}}">
    <a href="{{route('administrator-buku-saldo')}}">BUKU SALDO SIMPANAN</a>
  </li>
  <li class="{{setActive(['administrator-akumulasi-belanja','administrator-saldo-akumulasi-belanja'])}} submenu">
    <a href="#"><span>AKUMULASI BELANJA</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a class="{{setActive(['administrator-akumulasi-belanja'])}}" href="{{route('administrator-akumulasi-belanja')}}">&nbsp;&nbsp;&nbsp;Laporan Akumulasi</a></li>
      <li><a class="{{setActive(['administrator-saldo-akumulasi-belanja'])}}" href="{{route('administrator-saldo-akumulasi-belanja')}}">&nbsp;&nbsp;&nbsp;Saldo Akumulasi</a></li>
    </ul>
  </li>
  <li class="{{setActive(['administrator-kartu-anggota'])}}">
    <a href="{{route('administrator-kartu-anggota')}}">KARTU ANGGOTA</a>
  </li>
  <!-- <li class="submenu">
    <a href="#"><span>PRODUK ONLINE</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{url('/administrator/harga-ppob')}}">&nbsp;&nbsp;&nbsp;Produk PPOB</a></li>
      <li><a href="{{url('/administrator/produk')}}">&nbsp;&nbsp;&nbsp;Produk Barang</a></li>
    </ul>
  </li> -->
  <!-- <?php $pesanans = App\Pesanan::whereBetWeen('status',[2,3])->where('aktif', 1)->get(); ?>
  <li class="submenu">
    <a href="#"><span>PEMESANAN PRODUK</span> &nbsp;@if(count($pesanans) > 0)<span class="noti-dot active"></span>@endif <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <?php $dipesan = App\Pesanan::where('status',2)->where('aktif', 1)->sum('aktif'); ?>
      <li><a href="{{route('administrator-permintaan-barang')}}">&nbsp;&nbsp;&nbsp;Barang dipesan @if($dipesan > 0)<span class="badge bg-primary pull-right">{{ $dipesan }}</span>@endif</a></li>
      <?php $kirim = App\Pesanan::where('status',3)->where('aktif', 1)->sum('aktif'); ?>
      <li><a href="{{route('administrator-kirim-barang')}}">&nbsp;&nbsp;&nbsp;Barang dikirim @if($kirim > 0)<span class="badge bg-primary pull-right">{{ $kirim }}</span>@endif</a></li>
    </ul>
  </li>
  <li class="active submenu">
    <a href="#"><span>LAPORAN PENJUALAN</span> &nbsp;<span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{route('administrator-laporan-ppob')}}">&nbsp;&nbsp;&nbsp;Laporan PPOB</a></li>
      <li><a href="{{route('administrator-laporan-barang')}}">&nbsp;&nbsp;&nbsp;Laporan Barang</a></li>
       <li><a href="{{route('administrator-laporan-penjualan')}}">&nbsp;&nbsp;&nbsp;Laporan Penjualan</a></li>
    </ul>
  </li>
  <?php $depo = App\Deposit::where('status', 1)->where('aktif', 1)->get(); ?>
  <li class="active submenu">
    <a href="#"><span>KONFIRMASI DEPOSIT</span>&nbsp;@if(count($depo) > 0)<span class="noti-dot active"></span>@endif <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{route('konfirmasi-deposit')}}">&nbsp;&nbsp;&nbsp;Konfirmasi Deposit @if(count($depo) > 0)<span class="badge bg-primary pull-right">{{ count($depo) }}</span>@endif</a></li>
      <li><a href="{{route('laporan-deposit')}}">&nbsp;&nbsp;&nbsp;Laporan Deposit</a></li>
    </ul>
  </li>
  <li class="active submenu">
    <a href="#"><span>BUKU SALDO ANGGOTA</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{url('/administrator/buku-saldo')}}">&nbsp;&nbsp;&nbsp;Buku Saldo Simpanan</a></li>
      <li><a href="{{route('buku-saldo-transaksi-anggota')}}">&nbsp;&nbsp;&nbsp;Buku Saldo Transaksi</a></li>
    </ul>
  </li>
  <li class="active submenu">
    <a href="#"><span>SUPLAYER</span><span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{route('topup-suplayer')}}">&nbsp;&nbsp;&nbsp;Topup Saldo Suplayer</a></li>
      <li><a href="{{route('buku-saldo-suplayer')}}">&nbsp;&nbsp;&nbsp;Buku Saldo Suplayer</a></li>
    </ul>
  </li> -->

  <li class="{{setActive(['/administrator/statistik-akumulasi','/administrator/statistik-simpanan','/administrator/statistik-jenis-kelamin','/administrator/statistik-kelurahan','/administrator/statistik-kecamatan','/administrator/statistik-kabupaten'])}} submenu">
    <a href="#"><span>STATISTIK</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a class="{{setActive(['/administrator/statistik-akumulasi'])}}" href="{{route('/administrator/statistik-akumulasi')}}">&nbsp;&nbsp;&nbsp;Statistik Penjualan</a></li>
      <li><a class="{{setActive(['/administrator/statistik-simpanan'])}}" href="{{route('/administrator/statistik-simpanan')}}">&nbsp;&nbsp;&nbsp;Statistik Simpanan</a></li>
      <li><a class="{{setActive(['/administrator/statistik-jenis-kelamin'])}}" href="{{route('/administrator/statistik-jenis-kelamin')}}">&nbsp;&nbsp;&nbsp;Statistik Jenis Kelamin</a></li>
      <li><a class="{{setActive(['/administrator/statistik-kelurahan'])}}" href="{{route('/administrator/statistik-kelurahan')}}">&nbsp;&nbsp;&nbsp;Statistik Kelurahan</a></li>
      <li><a class="{{setActive(['/administrator/statistik-kecamatan'])}}" href="{{route('/administrator/statistik-kecamatan')}}">&nbsp;&nbsp;&nbsp;Statistik Kecamatan</a></li>
      <li><a class="{{setActive(['/administrator/statistik-kabupaten'])}}" href="{{route('/administrator/statistik-kabupaten')}}">&nbsp;&nbsp;&nbsp;Statistik Kabupaten</a></li>
    </ul>
  </li>
  {{--<li class="active submenu">
    <a href="#"><span>TRANSAKSI</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{route('belanja')}}">&nbsp;&nbsp;&nbsp;Belanja Barang</a></li>
      <li><a href="{{route('admin-transaksi-pulsa')}}">&nbsp;&nbsp;&nbsp;Pulsa HP</a></li>
      <li><a href="{{route('admin-voucher-listrik')}}">&nbsp;&nbsp;&nbsp;Voucher Listrik</a></li>
      <li><a href="{{route('admin-paket-data-internet')}}">&nbsp;&nbsp;&nbsp;Paket Data</a></li>
      <li><a href="{{route('admin-paket-sms')}}">&nbsp;&nbsp;&nbsp;Paket SMS/Telp</a></li>
      <li><a href="{{route('admin-transfer-pulsa')}}">&nbsp;&nbsp;&nbsp;Transfer Pulsa</a></li>
      <li><a href="{{route('admin-saldo-ojek')}}">&nbsp;&nbsp;&nbsp;Saldo Ojek</a></li>
      <li><a href="{{url('/administrator/data-transaksi-ppob')}}">&nbsp;&nbsp;&nbsp;Data Transaksi PPOB</a></li>
    </ul>
  </li>--}}
  {{--<li class="active submenu">
    <a href="#"><span>DEPOSIT</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{route('topup-deposit')}}">&nbsp;&nbsp;&nbsp;Topup Saldo</a></li>
      <li><a href="{{url('/administrator/my-buku-saldo')}}">&nbsp;&nbsp;&nbsp;Buku Saldo Simpanan</a></li>
      <li><a href="{{route('my-buku-saldo-transaksi')}}">&nbsp;&nbsp;&nbsp;Buku Saldo Transaksi</a></li>
      <!-- <li><a href="{{route('data.topup')}}">&nbsp;&nbsp;&nbsp;Laporan Deposit</a></li> -->
    </ul>--}}
  <li class="{{setActive(['/administrator/profil','/administrator/my-akumulasi-belanja','/administrator/kartu','/administrator/ganti-password'])}} submenu">
    <a href="#"><span>DATA SAYA</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a class="{{setActive(['/administrator/profil'])}}" href="{{route('/administrator/profil')}}">&nbsp;&nbsp;&nbsp;Profil</a></li>
      <!-- <li><a href="{{url('/administrator/alamat-kirim')}}">&nbsp;&nbsp;&nbsp;Alamat Kirim</a></li> -->
      <!-- <li><a href="{{url('/administrator/simpanan')}}">&nbsp;&nbsp;&nbsp;Simpanan</a></li> -->
      <li><a class="{{setActive(['/administrator/my-akumulasi-belanja'])}}" href="{{route('/administrator/my-akumulasi-belanja')}}">&nbsp;&nbsp;&nbsp;Akumulasi Belanja</a></li>
      <li><a class="{{setActive(['/administrator/kartu'])}}" href="{{route('/administrator/kartu')}}">&nbsp;&nbsp;&nbsp;Kartu Saya</a></li>
      <li><a class="{{setActive(['/administrator/ganti-password'])}}" href="{{route('/administrator/ganti-password')}}">&nbsp;&nbsp;&nbsp;Ganti Password</a></li>
    </ul>
  </li>
  {{--<li class="active">
    <a href="{{route('administrator.pengaturan')}}">PENGATURAN</a>
  </li>--}}
  <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">KELUAR</a></li>
</ul>
</div>
    </div>
</div>
