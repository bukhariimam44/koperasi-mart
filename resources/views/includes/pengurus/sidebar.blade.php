<div class="sidebar" id="sidebar">
    <div class="sidebar-inner slimscroll">
<div id="sidebar-menu" class="sidebar-menu">
<ul>
  <li class="{{setActive(['/pengurus/home'])}}">
    <a href="{{route('/pengurus/home')}}">DASHBOARD PENGURUS</a>
  </li>
  <li class="{{setActive(['pengurus.data-pendidikan','pengurus.data-komunitas','pengurus.data-kelurahan','pengurus.data-kecamatan','pengurus.data-kabupaten','pengurus.data-propinsi','pengurus.data-pendapatan','pengurus.data-pekerjaan','pengurus.data-jenis-simpanan','pengurus.data-rekening'])}} submenu">
    <a href="#"><span>DATA MASTER</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a class="{{setActive(['pengurus.data-pendidikan'])}}" href="{{route('pengurus.data-pendidikan')}}">&nbsp;&nbsp;&nbsp;Data Pendidikan</a></li>
      <li><a class="{{setActive(['pengurus.data-komunitas'])}}" href="{{route('pengurus.data-komunitas')}}">&nbsp;&nbsp;&nbsp;Data Komunitas</a></li>
      <li><a class="{{setActive(['pengurus.data-kelurahan'])}}" href="{{route('pengurus.data-kelurahan')}}">&nbsp;&nbsp;&nbsp;Data Kelurahan</a></li>
      <li><a class="{{setActive(['pengurus.data-kecamatan'])}}" href="{{route('pengurus.data-kecamatan')}}">&nbsp;&nbsp;&nbsp;Data Kecamatan</a></li>
      <li><a class="{{setActive(['pengurus.data-kabupaten'])}}" href="{{route('pengurus.data-kabupaten')}}">&nbsp;&nbsp;&nbsp;Data Kabupaten</a></li>
      <li><a class="{{setActive(['pengurus.data-propinsi'])}}" href="{{route('pengurus.data-propinsi')}}">&nbsp;&nbsp;&nbsp;Data Propinsi</a></li>
      <li><a class="{{setActive(['pengurus.data-pendapatan'])}}" href="{{route('pengurus.data-pendapatan')}}">&nbsp;&nbsp;&nbsp;Data Pendapatan</a></li>
      <li><a class="{{setActive(['pengurus.data-pekerjaan'])}}" href="{{route('pengurus.data-pekerjaan')}}">&nbsp;&nbsp;&nbsp;Data Pekerjaan</a></li>
      <li><a class="{{setActive(['pengurus.data-jenis-simpanan'])}}" href="{{route('pengurus.data-jenis-simpanan')}}">&nbsp;&nbsp;&nbsp;Jenis Simpanan</a></li>
      <li><a class="{{setActive(['pengurus.data-rekening'])}}" href="{{route('pengurus.data-rekening')}}">&nbsp;&nbsp;&nbsp;Data Rekening</a></li>
    </ul>
  </li>
  <li class="{{setActive(['/pengurus/data-anggota','/pengurus/data-group','/pengurus/data-karyawan'])}} submenu">
    <a href="#"><span>DATA PENGGUNA</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a class="{{setActive(['/pengurus/data-anggota'])}}" href="{{route('/pengurus/data-anggota')}}">&nbsp;&nbsp;&nbsp;Data Anggota</a></li>
      <li><a class="{{setActive(['/pengurus/data-group'])}}" href="{{route('/pengurus/data-group')}}">&nbsp;&nbsp;&nbsp;Data Group</a></li>
      <li><a class="{{setActive(['/pengurus/data-karyawan'])}}" href="{{route('/pengurus/data-karyawan')}}">&nbsp;&nbsp;&nbsp;Data Karyawan</a></li>
      <!-- <li><a href="{{url('/administrator/simpanan-jatuh-tempo')}}">&nbsp;&nbsp;&nbsp;Data Jatuh Tempo</a></li> -->
    </ul>
  </li>
  <li class="{{setActive(['/pengurus/data-simpanan'])}}">
    <a href="{{route('/pengurus/data-simpanan')}}">SIMPANAN ANGGOTA</a>
  </li>
  <li class="{{setActive(['/pengurus/status-simpanan-wajib'])}}">
    <a href="{{route('/pengurus/status-simpanan-wajib')}}">STATUS SIMPANAN WAJIB</a>
  </li>
  <li class="{{setActive(['/pengurus/akumulasi-belanja','/pengurus/saldo-akumulasi-belanja'])}} submenu">
    <a href="#"><span>AKUMULASI BELANJA</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a class="{{setActive(['/pengurus/akumulasi-belanja'])}}" href="{{route('/pengurus/akumulasi-belanja')}}">&nbsp;&nbsp;&nbsp;Laporan Akumulasi</a></li>
      <li><a class="{{setActive(['/pengurus/saldo-akumulasi-belanja'])}}" href="{{route('/pengurus/saldo-akumulasi-belanja')}}">&nbsp;&nbsp;&nbsp;Saldo Akumulasi</a></li>
    </ul>
  </li>
  <li class="{{setActive(['/pengurus/kartu-anggota'])}}">
    <a href="{{route('/pengurus/kartu-anggota')}}">KARTU ANGGOTA</a>
  </li>
  <!-- <li class="active submenu">
    <a href="#"><span>PRODUK ONLINE</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{route('pengurus-harga-ppob')}}">&nbsp;&nbsp;&nbsp;Produk PPOB</a></li>
      <li><a href="{{route('pengurus-harga-barang')}}">&nbsp;&nbsp;&nbsp;Produk Barang</a></li>
    </ul>
  </li> -->
  <!-- <li class="active submenu">
    <a href="#"><span>LAPORAN PENJUALAN</span> &nbsp;<span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{route('pengurus-laporan-ppob')}}">&nbsp;&nbsp;&nbsp;Laporan PPOB</a></li>
      <li><a href="{{route('pengurus-laporan-barang')}}">&nbsp;&nbsp;&nbsp;Laporan Barang</a></li>
    </ul>
  </li> -->
  <!-- <li class="active submenu">
    <a href="#"><span>TRANSAKSI</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">

      <li><a href="{{route('pengurus-belanja')}}">&nbsp;&nbsp;&nbsp;Belanja Barang</a></li>
      <li><a href="{{url('/pengurus/pulsa')}}">&nbsp;&nbsp;&nbsp;Pulsa HP</a></li>
      <li><a href="{{url('/pengurus/paket-data-internet')}}">&nbsp;&nbsp;&nbsp;Paket Data Internet</a></li>
      <li><a href="{{url('/pengurus/voucher-listrik')}}">&nbsp;&nbsp;&nbsp;Voucher Listrik</a></li>
      <li><a href="{{url('/pengurus/saldo-ojek')}}">&nbsp;&nbsp;&nbsp;Saldo Ojek</a></li>
      <li><a href="{{url('/pengurus/paket-sms')}}">&nbsp;&nbsp;&nbsp;Paket SMS/Telp</a></li>
      <li><a href="{{url('/pengurus/transfer-pulsa')}}">&nbsp;&nbsp;&nbsp;Transfer Pulsa</a></li>
      <li><a href="#">&nbsp;&nbsp;&nbsp;Tagihan Listrik</a></li> 
      <li><a href="{{url('pengurus/data-transaksi-ppob')}}">&nbsp;&nbsp;&nbsp;Data Transaksi PPOB</a></li>
      <li><a href="{{route('pengurus-data-transaksi-belanja')}}">&nbsp;&nbsp;&nbsp;Data Transaksi Belanja</a></li>
    </ul>
  </li> -->
<!-- <li class="active submenu">
  <a href="#"><span>SUPLAYER</span> <span class="menu-arrow"></span></a>
  <ul class="list-unstyled" style="display: none;">
    <li><a href="{{route('laporan-topup-suplayer')}}">&nbsp;&nbsp;&nbsp;Laporan Deposit Suplayer</a></li>
    <li><a href="{{route('pengurus-buku-saldo-suplayer')}}">&nbsp;&nbsp;&nbsp;Buku Saldo Suplayer</a></li>
  </ul>
  </li> -->
  <li class="{{setActive(['/pengurus/buku-saldo'])}}">
    <a href="{{route('/pengurus/buku-saldo')}}">BUKU SALDO SIMPANAN</a>
  </li>
  <!-- <li class="active submenu">
    <a href="#"><span>BUKU SALDO ANGGOTA</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{url('/pengurus/buku-saldo')}}">&nbsp;&nbsp;&nbsp;Buku Saldo Simpanan</a></li>
      <li><a href="{{route('buku-saldo-transaksi-pengurus')}}">&nbsp;&nbsp;&nbsp;Buku Saldo Transaksi</a></li>
    </ul>

</li> -->


  <li class="{{setActive(['/pengurus/statistik-akumulasi','/pengurus/statistik-jenis-kelamin','/pengurus/statistik-simpanan','/pengurus/statistik-kelurahan','/pengurus/statistik-kecamatan','/pengurus/statistik-kabupaten'])}} submenu">
    <a href="#"><span>STATISTIK</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a class="{{setActive(['/pengurus/statistik-akumulasi'])}}" href="{{route('/pengurus/statistik-akumulasi')}}">&nbsp;&nbsp;&nbsp;Statistik Penjualan</a></li>
      <li><a class="{{setActive(['/pengurus/statistik-jenis-kelamin'])}}" href="{{route('/pengurus/statistik-jenis-kelamin')}}">&nbsp;&nbsp;&nbsp;Statistik Jenis Kelamin</a></li>
      <li><a class="{{setActive(['/pengurus/statistik-simpanan'])}}" href="{{route('/pengurus/statistik-simpanan')}}">&nbsp;&nbsp;&nbsp;Statistik Simpanan</a></li>
      <li><a class="{{setActive(['/pengurus/statistik-kelurahan'])}}" href="{{route('/pengurus/statistik-kelurahan')}}">&nbsp;&nbsp;&nbsp;Statistik Kelurahan</a></li>
      <li><a class="{{setActive(['/pengurus/statistik-kecamatan'])}}" href="{{route('/pengurus/statistik-kecamatan')}}">&nbsp;&nbsp;&nbsp;Statistik Kecamatan</a></li>
      <li><a class="{{setActive(['/pengurus/statistik-kabupaten'])}}" href="{{route('/pengurus/statistik-kabupaten')}}">&nbsp;&nbsp;&nbsp;Statistik Kabupaten</a></li>
    </ul>
  </li>
  <!-- <li class="active submenu">
    <a href="#"><span>BUKU SALDO</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{route('topup-deposit-pengurus')}}">&nbsp;&nbsp;&nbsp;Topup Saldo</a></li>
      <li><a href="{{url('/pengurus/my-buku-saldo')}}">&nbsp;&nbsp;&nbsp;Buku Saldo Simpanan</a></li>
      <li><a href="{{url('/pengurus/buku-saldo-transaksi')}}">&nbsp;&nbsp;&nbsp;Buku Saldo Transaksi</a></li>
    </ul>
  </li> -->
  <li class="{{setActive(['/pengurus/profil','/pengurus/simpanan','/pengurus/my-akumulasi-belanja','/pengurus/kartu','/pengurus/ganti-password'])}} submenu">
    <a href="#"><span>DATA SAYA</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a class="{{setActive(['/pengurus/profil'])}}" href="{{url('/pengurus/profil')}}">&nbsp;&nbsp;&nbsp;Profil</a></li>
      <li><a class="{{setActive(['/pengurus/simpanan'])}}" href="{{url('/pengurus/simpanan')}}">&nbsp;&nbsp;&nbsp;Simpanan</a></li>
      <li><a class="{{setActive(['/pengurus/my-akumulasi-belanja'])}}" href="{{url('/pengurus/my-akumulasi-belanja')}}">&nbsp;&nbsp;&nbsp;Akumulasi Belanja</a></li>
      <li><a class="{{setActive(['/pengurus/kartu'])}}" href="{{url('/pengurus/kartu')}}">&nbsp;&nbsp;&nbsp;Kartu Saya</a></li>
      <li><a class="{{setActive(['/pengurus/ganti-password'])}}" href="{{url('/pengurus/ganti-password')}}">&nbsp;&nbsp;&nbsp;Ganti Password</a></li>
    </ul>
  </li>
  <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">KELUAR</a></li>
</ul>
</div>
    </div>
</div>
