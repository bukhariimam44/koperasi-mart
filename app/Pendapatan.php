<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pendapatan extends Model
{
  protected $fillable = [
      'id','name','aktif','created_at','updated_at'
  ];
}
