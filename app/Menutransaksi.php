<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menutransaksi extends Model
{
  protected $fillable = [
      'id','menu','route','aktif','created_at','updated_at'
  ];
}
