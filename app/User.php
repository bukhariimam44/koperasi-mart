<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'no_anggota','name','tgl_daftar','tpt_lahir','tgl_lahir','no_anggota','no_ks212','nik','sequence','npwp','email','agama','pendidikan','pekerjaan','statuskeluarga','nama_ibu','nama_ayah','telp','jenkel','alamat','kelurahan','kecamatan','kabupaten','komunitas','propinsi','pendapatan','pekerjaan','password','type','kartu','fotodiri','fotoktp','saldo','saldotransaksi','admin','aktif'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function pendidikan_id(){
      return $this->belongsTo('App\Pendidikan','pendidikan');
    }
    public function pekerjaan_id(){
      return $this->belongsTo('App\Pekerjaan','pekerjaan');
    }
    public function komunitas_id(){
      return $this->belongsTo('App\Komunitas','komunitas');
    }
    public function kelurahan_id(){
      return $this->belongsTo('App\Kelurahan','kelurahan');
    }
    public function kecamatan_id(){
      return $this->belongsTo('App\Kecamatan','kecamatan');
    }
    public function kabupaten_id(){
      return $this->belongsTo('App\Kabupaten','kabupaten');
    }
    public function propinsi_id(){
      return $this->belongsTo('App\Propinsi','propinsi');
    }
    public function pendapatan_id(){
      return $this->belongsTo('App\Pendapatan','pendapatan');
    }
    // public function user_id(){
    //   return $this->hasOne('App\Simpananadmin','user_id');
    // }
}
