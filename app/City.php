<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
  protected $fillable = [
      'id','city_id','province_id','province','type','city_name','postal_code'
  ];
}
