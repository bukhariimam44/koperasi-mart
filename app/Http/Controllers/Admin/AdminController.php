<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use \Khill\Lavacharts\DataTables\DataFactory;
use App\User;
use App\Daftar;
use App\Simpananadmin;
use App\Pendidikan;
use App\Komunitas;
use App\Kelurahan;
use App\Kecamatan;
use App\Kabupaten;
use App\Propinsi;
use App\Pendapatan;
use App\Pekerjaan;
use App\JenisSimpanan;
use Image;
use DB;
use Log;
use Auth;
// use Charts;

class AdminController extends Controller
{
  public function __construct()
  {
      $this->middleware('admin');
  }
  public function dataadmin(Request $request){

    $type = $request->types;
    $noanggota = User::orderBy('no_anggota','DESC')->first();
    $nomor = (int)$noanggota->no_anggota+1;
    $sequence = substr($nomor,-6);
    $anggota = date('Ym').$sequence;
    $tanggal_lahir = date('Y-m-d', strtotime($request->tgl_lahir));
    $this->validate($request, [
          'fotoktp' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
          'fotodiri' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
    ]);

    DB::beginTransaction();
    try {
      $ktp = $anggota.'.'.$request->fotoktp->getClientOriginalExtension();
      $diri = $anggota.'.'.$request->fotodiri->getClientOriginalExtension();
      $user = User::create([
        'tgl_daftar'=>date('Y-m-d'),
        'name'=>$request->name,
        'tpt_lahir'=>$request->tmp_lahir,
        'tgl_lahir'=>$tanggal_lahir,
        'nik'=>$request->nik,
        'no_anggota'=>$anggota,
        'sequence'=>$sequence,
        'npwp'=>$request->npwp,
        'email'=>$request->email,
        'agama'=>$request->agama,
        'pendidikan'=>$request->pendidikan,
        'statuskeluarga'=>$request->statuskeluarga,
        'nama_ibu'=>$request->nama_ibu,
        'nama_ayah'=>$request->nama_ayah,
        'telp'=>$request->telp,
        'jenkel'=>$request->jenkel,
        'alamat'=>$request->alamat,
        'komunitas'=>$request->komunitas,
        'kelurahan'=>$request->kelurahan,
        'kecamatan'=>$request->kecamatan,
        'kabupaten'=>$request->kabupaten,
        'propinsi'=>$request->propinsi,
        'pendapatan'=>$request->pendapatan,
        'pekerjaan'=>$request->pekerjaan,
        'password'=>Hash::make($sequence),
        'type'=>$type,
        'fotoktp'=>$ktp,
        'fotodiri'=>$diri,
        'admin'=>$request->user()->id,
        'aktif'=>1,
        'saldo'=>0,
      ]);

      // $pokok = Simpanan::create([
      //   'sequence'=>$anggota,
      //   'tanggalSetor'=>date('Y-m-d'),
      //   'jenis_simpanan'=>1,
      //   'mutasi'=>'Tambah',
      //   'nominal'=>$request->pokok,
      //   'saldo'=>$request->pokok,
      //   'ket'=>'Simpanan pokok pendaftaran'
      // ]);
      // $wajib = Simpanan::create([
      //   'sequence'=>$anggota,
      //   'tanggalSetor'=>date('Y-m-d'),
      //   'jenis_simpanan'=>2,
      //   'mutasi'=>'Tambah',
      //   'nominal'=>$request->wajib,
      //   'saldo'=>$request->pokok+$request->wajib,
      //   'ket'=>'Simpanan wajib pendaftaran'
      // ]);
      // $sukarela = Simpanan::create([
      //   'sequence'=>$anggota,
      //   'tanggalSetor'=>date('Y-m-d'),
      //   'jenis_simpanan'=>3,
      //   'mutasi'=>'Tambah',
      //   'nominal'=>$request->sukarela,
      //   'saldo'=>$request->pokok+$request->wajib+$request->sukarela,
      //   'ket'=>'Simpanan sukarela pendaftaran'
      // ]);
      // $infaq = Simpanan::create([
      //   'sequence'=>$anggota,
      //   'tanggalSetor'=>date('Y-m-d'),
      //   'jenis_simpanan'=>4,
      //   'mutasi'=>'Tambah',
      //   'nominal'=>$request->infaq,
      //   'saldo'=>$request->pokok+$request->wajib+$request->sukarela+$request->infaq,
      //   'ket'=>'Simpanan infaq pendaftaran'
      // ]);
      // $wakaf = Simpanan::create([
      //   'sequence'=>$anggota,
      //   'tanggalSetor'=>date('Y-m-d'),
      //   'jenis_simpanan'=>5,
      //   'mutasi'=>'Tambah',
      //   'nominal'=>$request->wakaf,
      //   'saldo'=>$request->pokok+$request->wajib+$request->sukarela+$request->infaq+$request->wakaf,
      //   'ket'=>'Simpanan wakaf pendaftaran'
      // ]);

      $destination_foto =public_path('foto');
      $request->fotodiri->move($destination_foto, $diri);
      $destination_ktp =public_path('ktp');
      $request->fotoktp->move($destination_ktp, $ktp);

    } catch (\Exception $e) {
      Log::info('Gagal input Anggota:'.$e->getMessage());
      DB::rollback();
      flash()->overlay('Data gagal di tambahkan.','INFO');
      return redirect()->back();
    }
    DB::commit();
    flash()->overlay('Data berhasil di Tambahkan.','INFO');
    return redirect()->back();


  }
  public function toadmin($id){
    $user = User::find($id);
    $user->type = 'admin';
    $user->admin = Auth::user()->id;
    $user->update();
    return redirect()->back();
  }
  public function topengurus($id){
    $user = User::find($id);
    $user->type = 'pengurus';
    $user->admin = Auth::user()->id;
    $user->update();
    return redirect()->back();
  }
  public function toanggota($id){
    $user = User::find($id);
    $user->type = 'anggota';
    $user->admin = Auth::user()->id;
    $user->update();
    return redirect()->back();
  }

  //DAFTAR daftar-online
  public function todaftar(){
    return view('administrator.daftaronline');
  }
  public function actiondaftar(Request $request, $id){
    $datas = Daftar::find($id);
    $noanggota = User::orderBy('no_anggota','DESC')->first();
    $nomor = (int)$noanggota->no_anggota+1;
    $sequence = substr($nomor,-6);
    $anggota = date('Ym').$sequence;
    if ($request->action == 'proses') {
      DB::beginTransaction();
      try {
        $datas = Daftar::find($id);
        $saldo = $datas->pokok+$datas->wajib+$datas->investasi+$datas->sukarela+$datas->infaq+$datas->wakaf;
        $user = User::create([
          'tgl_daftar'=>date('Y-m-d'),
          'name'=>$datas->name,
          'tpt_lahir'=>$datas->tmp_lahir,
          'tgl_lahir'=>$datas->tgl_lahir,
          'nik'=>$datas->nik,
          'no_anggota'=>$anggota,
          'sequence'=>$sequence,
          'npwp'=>$datas->npwp,
          'email'=>$datas->email,
          'agama'=>$datas->agama,
          'pendidikan'=>$datas->pendidikan,
          'statuskeluarga'=>$datas->statuskeluarga,
          'nama_ibu'=>$datas->nama_ibu,
          'nama_ayah'=>$datas->nama_ayah,
          'telp'=>$datas->telp,
          'jenkel'=>$datas->jenkel,
          'alamat'=>$datas->alamat,
          'komunitas'=>$datas->komunitas,
          'kelurahan'=>$datas->kelurahan,
          'kecamatan'=>$datas->kecamatan,
          'kabupaten'=>$datas->kabupaten,
          'propinsi'=>$datas->propinsi,
          'pendapatan'=>$datas->pendapatan,
          'pekerjaan'=>$datas->pekerjaan,
          'password'=>Hash::make($sequence),
          'type'=>'anggota',
          'fotoktp'=>$datas->fotoktp,
          'fotodiri'=>$datas->fotodiri,
          'admin'=>Auth::user()->id,
          'aktif'=>1,
          'saldo'=>$saldo,
        ]);

        $pokok = Simpananadmin::create([
          'no_trx'=>date('YmdHis'),
          'no_anggota'=>$anggota,
          'tgl_setor'=>date('Y-m-d'),
          'jenis_simpanan'=>1,
          'mutasi'=>'Kredit',
          'nominal'=>$datas->pokok,
          'saldo'=>$datas->pokok,
          'aktif'=>1,
          'petugas'=>$request->user()->id,
          'ket'=>'Simpanan pokok pendaftaran'
        ]);
        $wajib = Simpananadmin::create([
          'no_trx'=>date('YmdHis')+1,
          'no_anggota'=>$anggota,
          'tgl_setor'=>date('Y-m-d'),
          'jenis_simpanan'=>2,
          'mutasi'=>'Kredit',
          'nominal'=>$datas->wajib,
          'saldo'=>$datas->pokok+$datas->wajib,
          'aktif'=>1,
          'petugas'=>$request->user()->id,
          'ket'=>'Simpanan wajib pendaftaran'
        ]);
        $investasi = Simpananadmin::create([
          'no_trx'=>date('YmdHis')+2,
          'no_anggota'=>$anggota,
          'tgl_setor'=>date('Y-m-d'),
          'jenis_simpanan'=>6,
          'mutasi'=>'Kredit',
          'nominal'=>$datas->investasi,
          'saldo'=>$datas->pokok+$datas->wajib+$datas->investasi,
          'aktif'=>1,
          'petugas'=>$request->user()->id,
          'ket'=>'Simpanan wajib pendaftaran'
        ]);
        $sukarela = Simpananadmin::create([
          'no_trx'=>date('YmdHis')+3,
          'no_anggota'=>$anggota,
          'tgl_setor'=>date('Y-m-d'),
          'jenis_simpanan'=>3,
          'mutasi'=>'Kredit',
          'nominal'=>$datas->sukarela,
          'saldo'=>$datas->pokok+$datas->wajib+$datas->investasi+$datas->sukarela,
          'aktif'=>1,
          'petugas'=>$request->user()->id,
          'ket'=>'Simpanan sukarela pendaftaran'
        ]);
        $infaq = Simpananadmin::create([
          'no_trx'=>date('YmdHis')+4,
          'no_anggota'=>$anggota,
          'tgl_setor'=>date('Y-m-d'),
          'jenis_simpanan'=>4,
          'mutasi'=>'Kredit',
          'nominal'=>$datas->infaq,
          'saldo'=>$datas->pokok+$datas->wajib+$datas->investasi+$datas->sukarela+$datas->infaq,
          'aktif'=>1,
          'petugas'=>$request->user()->id,
          'ket'=>'Simpanan infaq pendaftaran'
        ]);
        $wakaf = Simpananadmin::create([
          'no_trx'=>date('YmdHis')+5,
          'no_anggota'=>$anggota,
          'tgl_setor'=>date('Y-m-d'),
          'jenis_simpanan'=>5,
          'mutasi'=>'Kredit',
          'nominal'=>$datas->wakaf,
          'saldo'=>$datas->pokok+$datas->wajib+$datas->investasi+$datas->sukarela+$datas->infaq+$datas->wakaf,
          'aktif'=>1,
          'petugas'=>$request->user()->id,
          'ket'=>'Simpanan wakaf pendaftaran'
        ]);
        $datas->status = 2;
        $datas->update();
      } catch (\Exception $e) {
        Log::info('Gagal input Anggota:'.$e->getMessage());
        DB::rollback();
        flash()->overlay('Data gagal di tambahkan.','INFO');
        return redirect()->back();
      }
      DB::commit();
      flash()->overlay('Data berhasil di Tambahkan.','INFO');
      return redirect()->back();

    }

    $datas->status = 3;
    $datas->update();
    flash()->overlay('Data gagal di tambahkan.','INFO');
    return redirect()->back();
  }

  public function pendidikan(Request $request){
    if ($request->action =='cari') {
      $pendidikans = Pendidikan::where('name',$request->name)->get();
      return view('administrator.dataPendidikan',compact('pendidikans'));
    }elseif ($request->action =='tambah') {
      if (Pendidikan::where('name',$request->name)->where('aktif',1)->first()) {
        flash()->overlay('Data Gagal input.','INFO');
        return redirect()->back();
      }
      $pendidikans = Pendidikan::create([
        'name'=>$request->name,
        'aktif'=>1,
        'admin'=>$request->user()->id
      ]);
      flash()->overlay('Data berhasil input.','INFO');
      return redirect()->back();
    }
    $pendidikans = Pendidikan::where('aktif',1)->get();
    return view('administrator.dataPendidikan',compact('pendidikans'));
  }
  public function editpendidikan(Request $request,$id){
    if ($request->action =='edit') {
      $pendidikans = Pendidikan::find($id);
      $pendidikans->name = $request->name;
      $pendidikans->admin = $request->user()->id;
      if ($pendidikans->update()) {
        flash()->overlay('Data berhasil Edit.','INFO');
        return redirect()->back();
      }
      flash()->overlay('Data Gagal Edit.','INFO');
      return redirect()->back();
    }
    $pendidikans = Pendidikan::find($id);
    $pendidikans->aktif = 0;
    $pendidikans->admin = $request->user()->id;
    if ($pendidikans->update()) {
      flash()->overlay('Data berhasil Hapus.','INFO');
      return redirect()->back();
    }
    flash()->overlay('Data Gagal Hapus.','INFO');
    return redirect()->back();
  }
  public function komunitas(Request $request){
    if ($request->action =='cari') {
      $komunitass = Komunitas::where('name',$request->name)->get();
      return view('administrator.dataKomunitas',compact('komunitass'));
    }elseif ($request->action =='tambah') {
      if (Komunitas::where('name',$request->name)->where('aktif',1)->first()) {
        flash()->overlay('Data Gagal input.','INFO');
        return redirect()->back();
      }
      $komunitass = Komunitas::create([
        'name'=>$request->name,
        'aktif'=>1,
        'admin'=>$request->user()->id
      ]);
      flash()->overlay('Data berhasil input.','INFO');
      return redirect()->back();
    }
    $komunitass = Komunitas::where('aktif',1)->get();
    return view('administrator.dataKomunitas',compact('komunitass'));
  }
  public function editkomunitas(Request $request,$id){
    if ($request->action =='edit') {
      $komunitass = Komunitas::find($id);
      $komunitass->name = $request->name;
      $komunitass->admin = $request->user()->id;
      if ($komunitass->update()) {
        flash()->overlay('Data berhasil Edit.','INFO');
        return redirect()->back();
      }
      flash()->overlay('Data Gagal Edit.','INFO');
      return redirect()->back();
    }
    $komunitass = Komunitas::find($id);
    $komunitass->aktif = 0;
    $komunitass->admin = $request->user()->id;
    if ($komunitass->update()) {
      flash()->overlay('Data berhasil Hapus.','INFO');
      return redirect()->back();
    }
    flash()->overlay('Data Gagal Hapus.','INFO');
    return redirect()->back();
  }
  public function kelurahan(Request $request){
    if ($request->action =='cari') {
      $kelurahans = Kelurahan::where('name',$request->name)->get();
      return view('administrator.dataKelurahan',compact('kelurahans'));
    }elseif ($request->action =='tambah') {
      if (Kelurahan::where('name',$request->name)->where('aktif',1)->first()) {
        flash()->overlay('Data Gagal input.','INFO');
        return redirect()->back();
      }
      $kelurahans = Kelurahan::create([
        'name'=>$request->name,
        'aktif'=>1,
        'admin'=>$request->user()->id
      ]);
      flash()->overlay('Data berhasil input.','INFO');
      return redirect()->back();
    }
    $kelurahans = Kelurahan::where('aktif',1)->get();
    return view('administrator.dataKelurahan',compact('kelurahans'));
  }
  public function editkelurahan(Request $request,$id){
    if ($request->action =='edit') {
      $kelurahans = Kelurahan::find($id);
      $kelurahans->name = $request->name;
      $kelurahans->admin = $request->user()->id;
      if ($kelurahans->update()) {
        flash()->overlay('Data berhasil Edit.','INFO');
        return redirect()->back();
      }
      flash()->overlay('Data Gagal Edit.','INFO');
      return redirect()->back();
    }
    $kelurahans = Kelurahan::find($id);
    $kelurahans->aktif = 0;
    $kelurahans->admin = $request->user()->id;
    if ($kelurahans->update()) {
      flash()->overlay('Data berhasil Hapus.','INFO');
      return redirect()->back();
    }
    flash()->overlay('Data Gagal Hapus.','INFO');
    return redirect()->back();
  }
  public function kecamatan(Request $request){
    if ($request->action =='cari') {
      $kecamatans = Kecamatan::where('name',$request->name)->get();
      return view('administrator.dataKecamatan',compact('kecamatans'));
    }elseif ($request->action =='tambah') {
      if (Kecamatan::where('name',$request->name)->where('aktif',1)->first()) {
        flash()->overlay('Data Gagal input.','INFO');
        return redirect()->back();
      }
      $kecamatans = Kecamatan::create([
        'name'=>$request->name,
        'aktif'=>1,
        'admin'=>$request->user()->id
      ]);
      flash()->overlay('Data berhasil input.','INFO');
      return redirect()->back();
    }
    $kecamatans = Kecamatan::where('aktif',1)->get();
    return view('administrator.dataKecamatan',compact('kecamatans'));
  }
  public function editkecamatan(Request $request,$id){
    if ($request->action =='edit') {
      $kecamatans = Kecamatan::find($id);
      $kecamatans->name = $request->name;
      $kecamatans->admin = $request->user()->id;
      if ($kecamatans->update()) {
        flash()->overlay('Data berhasil Edit.','INFO');
        return redirect()->back();
      }
      flash()->overlay('Data Gagal Edit.','INFO');
      return redirect()->back();
    }
    $kecamatans = Kecamatan::find($id);
    $kecamatans->aktif = 0;
    $kecamatans->admin = $request->user()->id;
    if ($kecamatans->update()) {
      flash()->overlay('Data berhasil Hapus.','INFO');
      return redirect()->back();
    }
    flash()->overlay('Data Gagal Hapus.','INFO');
    return redirect()->back();
  }
  public function kabupaten(Request $request){
    if ($request->action =='cari') {
      $kabupatens = Kabupaten::where('name',$request->name)->get();
      return view('administrator.dataKabupaten',compact('kabupatens'));
    }elseif ($request->action =='tambah') {
      if (Kabupaten::where('name',$request->name)->where('aktif',1)->first()) {
        flash()->overlay('Data Gagal input.','INFO');
        return redirect()->back();
      }
      $kabupatens = Kabupaten::create([
        'name'=>$request->name,
        'aktif'=>1,
        'admin'=>$request->user()->id
      ]);
      flash()->overlay('Data berhasil input.','INFO');
      return redirect()->back();
    }
    $kabupatens = Kabupaten::where('aktif',1)->get();
    return view('administrator.dataKabupaten',compact('kabupatens'));
  }
  public function editkabupaten(Request $request,$id){
    if ($request->action =='edit') {
      $kabupatens = Kabupaten::find($id);
      $kabupatens->name = $request->name;
      $kabupatens->admin = $request->user()->id;
      if ($kabupatens->update()) {
        flash()->overlay('Data berhasil Edit.','INFO');
        return redirect()->back();
      }
      flash()->overlay('Data Gagal Edit.','INFO');
      return redirect()->back();
    }
    $kabupatens = Kabupaten::find($id);
    $kabupatens->aktif = 0;
    $kabupatens->admin = $request->user()->id;
    if ($kabupatens->update()) {
      flash()->overlay('Data berhasil Hapus.','INFO');
      return redirect()->back();
    }
    flash()->overlay('Data Gagal Hapus.','INFO');
    return redirect()->back();
  }
  public function propinsi(Request $request){
    if ($request->action =='cari') {
      $propinsis = Propinsi::where('name',$request->name)->get();
      return view('administrator.dataPropinsi',compact('propinsis'));
    }elseif ($request->action =='tambah') {
      if (Propinsi::where('name',$request->name)->where('aktif',1)->first()) {
        flash()->overlay('Data Gagal input.','INFO');
        return redirect()->back();
      }
      $propinsis = Propinsi::create([
        'name'=>$request->name,
        'aktif'=>1,
        'admin'=>$request->user()->id
      ]);
      flash()->overlay('Data berhasil input.','INFO');
      return redirect()->back();
    }
    $propinsis = Propinsi::where('aktif',1)->get();
    return view('administrator.dataPropinsi',compact('propinsis'));
  }
  public function editpropinsi(Request $request,$id){
    if ($request->action =='edit') {
      $propinsis = Propinsi::find($id);
      $propinsis->name = $request->name;
      $propinsis->admin = $request->user()->id;
      if ($propinsis->update()) {
        flash()->overlay('Data berhasil Edit.','INFO');
        return redirect()->back();
      }
      flash()->overlay('Data Gagal Edit.','INFO');
      return redirect()->back();
    }
    $propinsis = Propinsi::find($id);
    $propinsis->aktif = 0;
    $propinsis->admin = $request->user()->id;
    if ($propinsis->update()) {
      flash()->overlay('Data berhasil Hapus.','INFO');
      return redirect()->back();
    }
    flash()->overlay('Data Gagal Hapus.','INFO');
    return redirect()->back();
  }
  public function pendapatan(Request $request){
    if ($request->action =='cari') {
      $pendapatans = Pendapatan::where('name',$request->name)->get();
      return view('administrator.dataPendapatan',compact('pendapatans'));
    }elseif ($request->action =='tambah') {
      if (Pendapatan::where('name',$request->name)->where('aktif',1)->first()) {
        flash()->overlay('Data Gagal input.','INFO');
        return redirect()->back();
      }
      $pendapatans = Pendapatan::create([
        'name'=>$request->name,
        'aktif'=>1,
        'admin'=>$request->user()->id
      ]);
      flash()->overlay('Data berhasil input.','INFO');
      return redirect()->back();
    }
    $pendapatans = Pendapatan::where('aktif',1)->get();
    return view('administrator.dataPendapatan',compact('pendapatans'));
  }
  public function editpendapatan(Request $request,$id){
    if ($request->action =='edit') {
      $pendapatans = Pendapatan::find($id);
      $pendapatans->name = $request->name;
      $pendapatans->admin = $request->user()->id;
      if ($pendapatans->update()) {
        flash()->overlay('Data berhasil Edit.','INFO');
        return redirect()->back();
      }
      flash()->overlay('Data Gagal Edit.','INFO');
      return redirect()->back();
    }
    $pendapatans = Pendapatan::find($id);
    $pendapatans->aktif = 0;
    $pendapatans->admin = $request->user()->id;
    if ($pendapatans->update()) {
      flash()->overlay('Data berhasil Hapus.','INFO');
      return redirect()->back();
    }
    flash()->overlay('Data Gagal Hapus.','INFO');
    return redirect()->back();
  }
  public function pekerjaan(Request $request){
    if ($request->action =='cari') {
      $pekerjaans = Pekerjaan::where('name',$request->name)->get();
      return view('administrator.dataPekerjaan',compact('pekerjaans'));
    }elseif ($request->action =='tambah') {
      if (Pekerjaan::where('name',$request->name)->where('aktif',1)->first()) {
        flash()->overlay('Data Gagal input.','INFO');
        return redirect()->back();
      }
      $pekerjaans = pekerjaan::create([
        'name'=>$request->name,
        'aktif'=>1,
        'admin'=>$request->user()->id
      ]);
      flash()->overlay('Data berhasil input.','INFO');
      return redirect()->back();
    }
    $pekerjaans = pekerjaan::where('aktif',1)->get();
    return view('administrator.dataPekerjaan',compact('pekerjaans'));
  }
  public function editpekerjaan(Request $request,$id){
    if ($request->action =='edit') {
      $pekerjaans = Pekerjaan::find($id);
      $pekerjaans->name = $request->name;
      $pekerjaans->admin = $request->user()->id;
      if ($pekerjaans->update()) {
        flash()->overlay('Data berhasil Edit.','INFO');
        return redirect()->back();
      }
      flash()->overlay('Data Gagal Edit.','INFO');
      return redirect()->back();
    }
    $pekerjaans = Pekerjaan::find($id);
    $pekerjaans->aktif = 0;
    $pekerjaans->admin = $request->user()->id;
    if ($pekerjaans->update()) {
      flash()->overlay('Data berhasil Hapus.','INFO');
      return redirect()->back();
    }
    flash()->overlay('Data Gagal Hapus.','INFO');
    return redirect()->back();
  }
  public function jenissimpanan(Request $request){
    if ($request->action =='cari') {
      $jenissimpanans = JenisSimpanan::where('name','LIKE','%'.$request->name.'%')->get();
      return view('administrator.jenisSimpanan',compact('jenissimpanans'));
    }elseif ($request->action =='tambah') {
      if (JenisSimpanan::where('name',$request->name)->where('aktif',1)->first()) {
        flash()->overlay('Data Gagal input.','INFO');
        return redirect()->back();
      }
      $jenissimpanans = JenisSimpanan::create([
        'name'=>$request->name,
        'aktif'=>1,
        'admin'=>$request->user()->id
      ]);
      flash()->overlay('Data berhasil input.','INFO');
      return redirect()->back();
    }
    $jenissimpanans = jenissimpanan::where('aktif',1)->get();
    return view('administrator.jenisSimpanan',compact('jenissimpanans'));
  }
  public function editjenissimpanan(Request $request,$id){
    if ($request->action =='edit') {
      $jenissimpanans = JenisSimpanan::find($id);
      $jenissimpanans->name = $request->name;
      $jenissimpanans->nominal = $request->nominal;
      $jenissimpanans->admin = $request->user()->id;
      if ($jenissimpanans->update()) {
        flash()->overlay('Data berhasil Edit.','INFO');
        return redirect()->back();
      }
      flash()->overlay('Data Gagal Edit.','INFO');
      return redirect()->back();
    }
    $jenissimpanans = JenisSimpanan::find($id);
    $jenissimpanans->aktif = 0;
    $jenissimpanans->admin = $request->user()->id;
    if ($jenissimpanans->update()) {
      flash()->overlay('Data berhasil Hapus.','INFO');
      return redirect()->back();
    }
    flash()->overlay('Data Gagal Hapus.','INFO');
    return redirect()->back();
  }
  public function kartu(){
    $anggota ="";
    $nama ="";
    $users = User::where('aktif',1)->where('type','<>','karyawan')->get();
    return view('administrator.kartuAnggota',compact('users','anggota','nama'));
  }
  public function uploads(Request $request){
    if ($request->action == 'upload') {
      $this->validate($request, [
            'fotodiri' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
      ]);

      $userss = User::find($request->ids);
        DB::beginTransaction();
        try {
          $users = User::find($request->ids);
          if ($request->file('fotodiri')) {
            // return 'OK';
            if ($users->fotodiri =='null' || $users->fotodiri =='') {
              $image = $request->file('fotodiri');
              $imageName = $image->getClientOriginalName();
              $fileName = date('YmdHis')."_".$imageName;
              $directory = public_path('/foto/');
              // return $directory;
              $imageUrl = $directory.$fileName;
              Image::make($image)->resize(400, 500)->save($imageUrl);
              $users->fotodiri = $fileName;
            }else {
              $destination_foto =public_path('foto/'.$users->fotodiri);
              if(file_exists($destination_foto)){
                          unlink($destination_foto);
              }
              $image = $request->file('fotodiri');
              $imageName = $image->getClientOriginalName();
              $fileName = date('YmdHis')."_".$imageName;
              $directory = public_path('/foto/');

              $imageUrl = $directory.$fileName;
              // return $imageUrl;
              Image::make($image)->resize(400, 500)->save($imageUrl);
              $users->fotodiri = $fileName;
            }

          }
          $users->save();
        } catch (\Exception $e) {
          Log::info('Gagal upload kartu:'.$e->getMessage());
          DB::rollback();
          flash()->overlay('Kartu gagal di Upload.','INFO');
          return redirect()->back();
        }
        DB::commit();
        flash()->overlay('Kartu Berhasil di Upload.','INFO');
        return redirect()->back();

    }
    $anggota = $request->no_anggota;
    $nama = $request->name;
    $users = User::where('no_anggota','LIKE','%'.$anggota.'%')->where('name','LIKE','%'.$nama.'%')->where('aktif',1)->where('type','<>','karyawan')->get();
    return view('administrator.kartuAnggota',compact('users','anggota','nama'));
  }
}
