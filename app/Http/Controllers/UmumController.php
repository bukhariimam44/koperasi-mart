<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Daftar;
use DB;
use Log;
use Validator;
use App\Datatransaksippobs;
use App\Bukusaldotransaksi;
use App\User;

class UmumController extends Controller
{
    public function upload_user(Request $request){
      if ($request->action =='csv') {
        $validatedData = $request->validate([
        'upload' => 'required'
        ]);
        $ktp = '12345.'.$request->upload->getClientOriginalExtension();
        $sequence = substr($ktp, -3);
        if ($sequence !=='csv') {
            flash()->overlay('Gagal, Data Upload harus CSV File.', 'INFO');
            return redirect()->back();
        }
        $upload = $request->file('upload');
        $filePath = $upload->getRealPath();
        $file = fopen($filePath, 'r');
        $header = fgetcsv($file);
        $escapedHeader=[];
        //validate
        foreach ($header as $key => $value) {
            $lheader=strtolower($value);
            $escapedItem=preg_replace('/[^a-z]/', '', $lheader);
            array_push($escapedHeader, $escapedItem);
        }
        //looping through othe columns
        while ($columns=fgetcsv($file)) {
            if ($columns[0]=="") {
                continue;
            }
            //trim data
            foreach ($columns as $key => &$value) {
                $value=$value;
            }
            // return $columns;
            $data = array_combine($escapedHeader, $columns);
            // setting type
            foreach ($data as $key => &$value) {
                $value=($key=="noanggota" || $key=="name" || $key=="telp")?(string)$value: (string)$value;
            }
            // return $data;
            // Table update
            $att=substr($data['noanggota'], 0,6);;
            $anggota=substr($data['noanggota'], -4);
            $no_anggota = $att.'00'.$anggota;
            $sequence = substr($no_anggota, 6);
            $name=$data['name'];
            $telp=$data['telp'];
            $user = User::firstOrNew(['no_anggota'=>$no_anggota,'name'=>$name, 'telp'=>$telp]);
            $user->no_anggota	=$no_anggota;
            $user->name = $name;
            $user->sequence = $sequence;
            $user->telp = $telp;
            $user->admin = $request->user()->id;  
            $user->save();
        }
        flash()->overlay('CSV Berhasil di Upload.', 'INFO');
        return 'berhasil';
    }
      return view('upload_user');
    }
    
    public function index(){
      $title = "";
      return view('welcome', compact('title'));
    }
    public function profil(){
      $title = "Profil";
      return view('profil', compact('title'));
    }
    public function organisasi(){
      $title = "Struktur Organisasi";
      return view('organisasi', compact('title'));
    }
    public function kontak(){
      $title = "Kontak";
      return view('contact', compact('title'));
    }
    public function daftar(){
      $title = "Daftar";
      return view('daftar', compact('title'));
    }
    public function create(Request $request){
      $messsages = array(
      		'email.unique'=>'Email sudah di gunakan',
      		'nik.unique'=>'No KTP sudah di gunakan',
	     );
       $rules = array(
            'email'=>'required|max:100|email|unique:users,email',
            'name'=>'required|max:100',
            'nik'=>'required|max:50|unique:users,nik',
            'fotoktp' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'fotodiri' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
      );
      $validator = $request->validate($rules,$messsages);
      $random = rand(1000,100000);
    DB::beginTransaction();
    try {
      $ktp = $request->nik.'.'.$request->fotoktp->getClientOriginalExtension();
      $diri = $request->nik.'.'.$request->fotodiri->getClientOriginalExtension();
      $daftars = Daftar::create([
        'tgl_daftar'=>date('Y-m-d'),
        'no_anggota'=>$request->no_anggota,
        'name'=>$request->name,
        'tpt_lahir'=>$request->tpt_lahir,
        'tgl_lahir'=>$request->tgl_lahir,
        'nik'=>$request->nik,
        'npwp'=>$request->npwp,
        'email'=>$request->email,
        'agama'=>$request->agama,
        'pendidikan'=>$request->pendidikan,
        'statuskeluarga'=>$request->statuskeluarga,
        'nama_ibu'=>$request->nama_ibu,
        'nama_ayah'=>$request->nama_ayah,
        'telp'=>$request->telp,
        'jenkel'=>$request->jenkel,
        'alamat'=>$request->alamat,
        'kelurahan'=>$request->kelurahan,
        'kecamatan'=>$request->kecamatan,
        'kabupaten'=>$request->kabupaten,
        'komunitas'=>$request->komunitas,
        'propinsi'=>$request->propinsi,
        'pendapatan'=>$request->pendapatan,
        'pekerjaan'=>$request->pekerjaan,
        'pokok'=>130000,
        'wajib'=>120000,
        'sukarela'=>$request->sukarela,
        'investasi'=>$request->investasi,
        'wakaf'=>$request->wakaf,
        'infaq'=>$request->infaq,
        'invoic'=>$random,
        'fotoktp'=>$ktp,
        'fotodiri'=>$diri,
        'admin'=>$request->admin,
        'status'=>1,
        'aktif'=>1,
      ]);
      $destination_foto =public_path('foto');
      $request->fotodiri->move($destination_foto, $diri);
      $destination_ktp =public_path('ktp');
      $request->fotoktp->move($destination_ktp, $ktp);

    } catch (\Exception $e) {
      Log::info('Gagal input Anggota:'.$e->getMessage());
      DB::rollback();
      flash()->overlay('Data gagal di tambahkan.','INFO');
      return redirect()->back();
    }
    DB::commit();
    flash()->overlay('Terim kasih atas kepercayaan anda untuk bergabung bersama KSSD
    Penyetoran Pertama Anggota Baru terdiri dari </br>
    1. Simpanan Pokok Rp. 130.000,-</br>
    2. Simpanan Wajib Rp. 120.000,-</br>
    3. Simpanan Sukarela Rp. ....</br>
    4. Investasi Rp. ....</br>
    5. Wakaf Rp.  . ....</br>
    6. Infaq/Sodaqoh Rp. ....</br></br>

    Silahkan transfer ke Bank Muamalat </br>
    No. Rekening (147) 3330  212  112</br></br>

    Bukti transfer disampaikan ke</br>
    Call Center :</br>WA 087 808 212 112','INFO');
    $title = "Daftar";
    $sukarela = $request->sukarela;
    $investasi=$request->investasi;
    $infaq = $request->infaq;
    $wakaf =$request->wakaf;
    return view('detailDaftar', compact('title','sukarela','investasi','infaq','wakaf','random'));

    }
    public function respon(Request $request){
      if ($request['ip'] == '103.247.10.253') {
        Log::info($request);
        if ($request['status'] == 4) {
          $datas = Datatransaksippobs::where('no_trx',$request['trxid_api'])->first();
          $datas->status = 'Berhasil';
          $datas->sn = $request['sn'];
          $datas->keterangan = $request['note'];
          $datas->update();
        }else {
          $datas = Datatransaksippobs::where('no_trx',$request['trxid_api'])->first();
          $datas->status='Gagal';
          $datas->keterangan = $request['note'];
          $saldo = User::find($datas->user_id);
          $saldoakhir = $saldo->saldotransaksi + $datas->nominal;
          $saldo->saldotransaksi = $saldoakhir;
          Bukusaldotransaksi::create([
            'user_id'=>$datas->user_id,
            'no_anggota'=>$datas->no_anggota,
            'no_trx'=>date('YmdHis'),
            'tgl_trx'=>date('Y-m-d'),
            'nominal'=>$datas->nominal,
            'saldo'=>$saldoakhir,
            'mutasi'=>'Kredit',
            'keterangan'=>$request['note'],
            'aktif'=>1,
          ]);
          $saldo->update();
          $datas->update();

        }
        return Response()->json([
          'result'=>'success',
          'message'=>$request['note']
        ]);
      }
    }
}
