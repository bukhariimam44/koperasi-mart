<?php

namespace App\Http\Controllers\Pengurus;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Simpananadmin;
use App\User;
use App\Kelurahan;
use App\Kecamatan;
use App\Kabupaten;
use App\Propinsi;
use App\Akumulasi;
use App\Menutransaksi;
use App\Toko;
use App\Product;
use App\Datatransaksippobs;
use App\Bukusaldotransaksi;
use App\Bank;
use App\Deposit;
use App\Website;
use App\City;
use App\Pesanan;
use App\Status;
use Image;
use Response;
use Auth;
use DB;
use Log;
use PDF;

class DataDiriController extends Controller
{
    public function __construct()
    {
        $this->middleware('pengurus');
    }

    public function profil(Request $request)
    {
        if ($request->fotodiri) {
            $this->validate($request, [
            'fotodiri' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
      ]);
            // $diri = date('YmdHis').'.'.$request->fotodiri->getClientOriginalExtension();
        }

        if ($request->tgl_lahir =="") {
            $tanggal_lahir = "";
        } else {
            $tanggal_lahir = date('Y-m-d', strtotime($request->tgl_lahir));
        }
        if ($request->action =='edit') {
            DB::beginTransaction();
            try {
                $user = User::find($request->user()->id);
                if ($request->file('fotodiri')) {
                    // return 'OK';
                    if ($user->fotodiri =='null' || $user->fotodiri =='') {
                        $image = $request->file('fotodiri');
                        $imageName = $image->getClientOriginalName();
                        $fileName = date('YmdHis')."_".$imageName;
                        $directory = public_path('/foto/');
                        // return $directory;
                        $imageUrl = $directory.$fileName;
                        Image::make($image)->resize(400, 500)->save($imageUrl);
                        $user->fotodiri = $fileName;
                    } else {
                        $destination_foto =public_path('foto/'.$user->fotodiri);
                        if (file_exists($destination_foto)) {
                            unlink($destination_foto);
                        }
                        $image = $request->file('fotodiri');
                        $imageName = $image->getClientOriginalName();
                        $fileName = date('YmdHis')."_".$imageName;
                        $directory = public_path('/foto/');

                        $imageUrl = $directory.$fileName;
                        // return $imageUrl;
                        Image::make($image)->resize(400, 500)->save($imageUrl);
                        $user->fotodiri = $fileName;
                    }
                }
                $user->tpt_lahir=$request->tpt_lahir;
                $user->tgl_lahir=$tanggal_lahir;
                $user->npwp=$request->npwp;
                $user->email=$request->email;
                $user->agama=$request->agama;
                $user->pendidikan=$request->pendidikan;
                $user->statuskeluarga=$request->statuskeluarga;
                $user->nama_ibu=$request->nama_ibu;
                $user->nama_ayah=$request->nama_ayah;
                $user->telp=$request->telp;
                $user->jenkel=$request->jenkel;
                $user->alamat=$request->alamat;
                $user->kelurahan=$request->kelurahan;
                $user->kecamatan=$request->kecamatan;
                $user->kabupaten=$request->kabupaten;
                $user->komunitas=$request->komunitas;
                $user->propinsi=$request->propinsi;
                $user->pendapatan=$request->pendapatan;
                $user->pekerjaan=$request->pekerjaan;
                $user->update();
            } catch (\Exception $e) {
                Log::info('Gagal Edit Profil:'.$e->getMessage());
                DB::rollback();
                flash()->overlay('Gagal Edit Profil.', 'INFO');
                return redirect()->back();
            }
            DB::commit();
            flash()->overlay('Data Profil berhasil di Edit.', 'INFO');
            return redirect()->back();
        }
        return view('pengurus.datadiri.profil');
    }
    public function simpanananggota(Request $request)
    {
        $nomor = $request->user()->no_anggota;
        $jenissim = $request->jenis_simpanan;
        $from = $request->dari;
        $to = $request->sampai;
        $dari = date('Y-m-d', strtotime($from));
        $ke = date('Y-m-d', strtotime($to));
        if ($request->action =='cari') {
            $simpanan = Simpananadmin::where('no_anggota', $nomor)->whereBetWeen('tgl_setor', [$dari,$ke])->where('jenis_simpanan', 'LIKE', '%'.$jenissim.'%')->where('aktif', 1)->orderBy('tgl_setor', 'ASC')->get();
            return view('pengurus.datadiri.simpanan', compact('nomor', 'simpanan', 'jenissim', 'from', 'to'));
        }
        $from = date('01-01-Y');
        $to = date('d-m-Y');
        $dari = date('Y-m-d', strtotime($from));
        $ke = date('Y-m-d', strtotime($to));
        $simpanan = Simpananadmin::whereBetWeen('tgl_setor', [$dari,$ke])->where('no_anggota', $request->user()->no_anggota)->where('aktif', 1)->orderBy('tgl_setor', 'ASC')->get();
        return view('pengurus.datadiri.simpanan', compact('nomor', 'simpanan', 'jenissim', 'from', 'to'));
    }
    public function bukusaldo(Request $request)
    {
        $anggota = $request->user()->no_anggota;
        $from = date('Y');
        $until = date('Y');
        // $dari = date('Y-m-d', strtotime($from));
        // $ke = date('Y-m-d', strtotime($until));
        if ($request->action =='cari') {
            $from =$request->dari;
            $until =$request->sampai;
            $dari = date('Y-m-d', strtotime($from));
            $ke = date('Y-m-d', strtotime($until));
        }
        $users = User::where('no_anggota', $anggota)->first();
        $simpananpokok = Simpananadmin::where('no_anggota', $anggota)->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 1)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $simpananwajib = Simpananadmin::where('no_anggota', $anggota)->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 2)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $simpanansukarela = Simpananadmin::where('no_anggota', $anggota)->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 3)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $simpananinvestasi = Simpananadmin::where('no_anggota', $anggota)->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 4)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $simpananwakaf = Simpananadmin::where('no_anggota', $anggota)->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 5)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $simpananinfaq = Simpananadmin::where('no_anggota', $anggota)->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 6)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $simpananshu = Simpananadmin::where('no_anggota', $anggota)->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 7)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $simpananlain = Simpananadmin::where('no_anggota', $anggota)->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 8)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        return view('pengurus.datadiri.bukusaldo', compact('simpananpokok', 'simpananwajib', 'simpanansukarela', 'simpananinvestasi', 'simpananwakaf', 'simpananinfaq', 'simpananshu', 'simpananlain', 'anggota', 'from', 'until', 'users'));
    }

    public function password()
    {
        return view('pengurus.datadiri.gantiPassword');
    }
    public function gantipassword(Request $request)
    {
        // Log::info(' ALERt :'.$request);

        if (\Hash::check($request->input('password_lm'), $request->user()->password)) {
            $user = User::find($request->user()->id);
            $user->password = bcrypt($request->input('password_br'));
            $user->update();
            flash()->overlay('Password Berhasil di rubah.', 'INFO');
            return redirect()->back();
        } else {
            flash()->overlay('Password Gagal di rubah<br>Password Lama Salah', 'INFO');
            return redirect()->back();
        }
    }
    public function kartu()
    {
        return view('pengurus.datadiri.kartuAnggota');
    }
    public function download($id)
    {
        $user = User::where('sequence', $id)->first();
        $pdf = PDF::loadView('administrator.report.pdf', compact('user'));
        return $pdf->download('kartu.pdf');
    }
    public function akumulasi(Request $request)
    {
        $from = date('01-01-Y');
        $to = date('d-m-Y');
        $dari = date('Y-m-d', strtotime($from));
        $ke = date('Y-m-d', strtotime($to));
        if ($request->action =='cari') {
            $from = $request->dari;
            $to = $request->sampai;
            $dari = date('Y-m-d', strtotime($from));
            $ke = date('Y-m-d', strtotime($to));
        }
        $akumulasis = Akumulasi::whereBetWeen('tgl_trx', [$dari,$ke])->where('aktif', 1)->where('no_kssd', $request->user()->no_anggota)->get();
        return view('pengurus.datadiri.akumulasi', compact('akumulasis', 'from', 'to'));
    }
    public function statistik(Request $request)
    {
        $berdasarkan = 'Kelurahan';
        $kelurahans=Kelurahan::where('aktif', 1)->get();
        if ($request->berdasarkan =='Kecamatan') {
            $berdasarkan = $request->berdasarkan;
            $kelurahans=Kecamatan::where('aktif', 1)->get();
        } elseif ($request->berdasarkan =='Kabupaten') {
            $berdasarkan = $request->berdasarkan;
            $kelurahans=Kabupaten::where('aktif', 1)->get();
        } elseif ($request->berdasarkan =='Jenis Kelamin') {
            $berdasarkan = $request->berdasarkan;
            $kelurahans=['Laki-Laki','Perempuan'];
        } elseif ($request->berdasarkan =='Pendidikan') {
            $berdasarkan = $request->berdasarkan;
            $kelurahans=Pendidikan::where('aktif', 1)->get();
        }
        return view('pengurus.statistik', compact('berdasarkan', 'kelurahans'));
    }
    public function belanja(Request $request)
    {
        $kode = "";
        $name = "";
        if ($request->action == 'cari') {
            $kode = $request->kode;
            $name = $request->name;
        } elseif ($request->action == 'beli') {
            $produk = Toko::find($request->ids);
            // return dd($produk);
            //PROPINSI
            $curl = curl_init();

            curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.rajaongkir.com/starter/province",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "key: da08d414a043e0001fd33b5b3ad29ad0"
        ),
      ));

            $response = curl_exec($curl);
            // return dd($response);
            $resarr =json_decode($response, true);
            $detail =  $resarr['rajaongkir']['results'];
            // return dd($detail);
            // $err = curl_error($curl);
            //
            // curl_close($curl);


            return view('pengurus.datadiri.pemesanan', compact('produk', 'kode', 'name', 'detail'));
        }
        $produks = Toko::where('aktif', 1)->where('kode', 'LIKE', '%'.$kode.'%')->where('name', 'LIKE', '%'.$name.'%')->orderBy('id', 'DESC')->get();
        return view('pengurus.datadiri.belanja', compact('produks', 'kode', 'name'));
    }
    public function pulsa(Request $request)
    {
        $menus = Menutransaksi::where('aktif', 1)->orderBy('id', 'ASC')->get();
        return view('pengurus.datadiri.pulsa', compact('menus'));
    }
    public function paketdata(Request $request)
    {
        $menus = Menutransaksi::where('aktif', 1)->orderBy('id', 'ASC')->get();
        return view('pengurus.datadiri.paket_data', compact('menus'));
    }
    public function voucher(Request $request)
    {
        $menus = Menutransaksi::where('aktif', 1)->orderBy('id', 'ASC')->get();
        return view('pengurus.datadiri.voucher_listrik', compact('menus'));
    }
    public function saldoojek(Request $request)
    {
        $menus = Menutransaksi::where('aktif', 1)->orderBy('id', 'ASC')->get();
        return view('pengurus.datadiri.saldo_ojek', compact('menus'));
    }
    public function paketsms(Request $request)
    {
        $menus = Menutransaksi::where('aktif', 1)->orderBy('id', 'ASC')->get();
        return view('pengurus.datadiri.paket_sms', compact('menus'));
    }
    public function transferpulsa(Request $request)
    {
        $menus = Menutransaksi::where('aktif', 1)->orderBy('id', 'ASC')->get();
        return view('pengurus.datadiri.transfer_pulsa', compact('menus'));
    }
    public function selectprovider(Request $request)
    {
        if ($request->ajax()) {
            Log::info('ID province = '.$request->provider);
            $pakets = Product::where('operator', $request->provider)->orderBy('jual', 'ASC')->get();
            Log::info('$detail = '.$pakets);
            $option = "";
            $option.="<option value=''>Pilih Nominal Pulsa</option>";
            foreach ($pakets as $key => $paket) {
                $option.="<option value='".$paket->code."'>".$paket->description."</option>";
            }
            return $option;
        }
    }
    public function selectpaket(Request $request)
    {
        if ($request->ajax()) {
            Log::info('ID province = '.$request->paket);
            $pakets = Product::where('code', $request->paket)->first();
            Log::info('$detail = '.$pakets);
            $option = "";
            $option.="<label>Rp. ".number_format($pakets->jual).",-</label>";
            return $option;
        }
    }
    public function prosesppob(Request $request)
    {
        if ($request->nomorhp =="" || $request->paket =="") {
            $params['code'] = '400';
            $params['messages'] = "<p class='alert alert-danger'>Isi form dengan lengkap</p>";
            return $params;
        }
        $this->validate($request, [
      'nomorhp' => 'required',
      'paket' => 'required'
  ]);
        $harga = Product::where('code', $request->paket)->first();
        if (Auth::user()->saldotransaksi > $harga->jual) {
            if (date('H') > 23 || date('H') < 1) {
                $params['code'] = '400';
                $params['messages'] = "<p class='alert alert-danger'>Saat ini sedang maintenance jam 23:00 s/d 01:00 WIB</p>";
                return $params;
            }
            $saldo = Auth::user()->saldotransaksi - $harga->jual;
            $trxid = date('ymdHis').rand(1,99);
            $nomorhp = $request->nomorhp;
            DB::beginTransaction();
            try {
                Datatransaksippobs::create([
                'user_id'=>$request->user()->id,
                'no_anggota'=>$request->user()->no_anggota,
                'no_trx'=>$trxid,
                'tgl_trx'=>date('Y-m-d'),
                'mutasi'=>'Kredit',
                'paket'=>$harga->description,
                'nominal'=>$harga->jual,
                'nta'=>$harga->price,
                'saldo'=>$saldo,
                'status'=>'Proses',
                'nomorhp'=>$nomorhp,
                'aktif'=>1,
                'keterangan'=>'Transaksi '.$harga->description,
                'created_by'=>$request->user()->id
              ]);
                User::find($request->user()->id)->update([
                  'saldotransaksi'=>$saldo
                ]);
            } catch (\Exception $e) {
                Log::info('Gagal Transaksi PPOB:'.$e->getMessage());
                DB::rollback();
                flash()->overlay('Transaksi GAGAL.', 'INFO');
                return redirect()->back();
            }
            DB::commit();
            // ????????
            $params['trxid']=$trxid;
            $params['nomor']=$request->nomorhp;
            $params['code']=$request->paket;
            $access_token = Website::find(1);
            $url = $access_token->url.'proses-agent';
            $client = new \GuzzleHttp\Client();
            $res = $client->request('POST', $url, [
                          'headers'=>[
                              'Accept' => 'application/json',
                              'Content-Type' => 'application/json',
                              'Authorization' => 'Bearer '.$access_token->token
                          ],
                                'body'=>json_encode($params)
                      ]);
            if ($res->getStatusCode() == 200) {
                $hasil =  json_decode($res->getBody()->getContents(), true);
                if ($hasil['result'] =='success') {
                    Bukusaldotransaksi::create([
                  'user_id'=>$request->user()->id,
                  'no_anggota'=>$request->user()->no_anggota,
                  'no_trx'=>$trxid,
                  'tgl_trx'=>date('Y-m-d'),
                  'nominal'=>$harga->jual,
                  'saldo'=>$saldo,
                  'mutasi'=>'Debet',
                  'keterangan'=>'Transaksi '.$harga->description.' '.$request->nomorhp,
                  'aktif'=>1,
                  'created_by'=>$request->user()->id,
                ]);
                    $url = route('pengurus-data-transaksi-ppob');
                    $nominal = number_format($harga->jual, 0, ",", ".");
                    $params['code'] = '200';
                    $params['messages'] = "<table class='table' id='table'>
                        <tr>
                          <td>Produk</td><td>: $harga->description </td>
                        </tr>
                        <tr>
                          <td>Nominal</td><td>: Rp $nominal </td>
                        </tr>
                        <tr>
                          <td>Nomor Pelanggan</td><td>: $request->nomorhp </td>
                        </tr>
                        <tr>
                          <td>Status</td><td>: Proses </td>
                        </tr>
                        <tr>
                          <td colspan='2'> Mohon di tunggu ...</td>
                        </tr>
                        <tr>
                          <td align='center' colspan='2'> <a href='$url'><button class='btn btn-primary'>Data Transaksi</button></a> </td>
                        </tr>
                      </table>";
                    return $params;
                } else {
                    $hapus = Datatransaksippobs::where('no_trx', $trxid)->first();
                    $hapus->delete();
                    $users = User::find($request->user()->id);
                    $users->saldotransaksi = $users->saldotransaksi + $harga->jual;
                    $users->update();
                    $params['code'] = '400';
                    $params['messages'] = "<p class='alert alert-danger'>Transaksi gagal silahkan di ulangi kembali</p>";
                    return $params;
                }
            } else {
                $params['code'] = '400';
                $params['messages'] = "<p class='alert alert-danger'>Gagalmenampilkan data</p>";
                return $params;
            }
        }
        $params['code'] = '400';
        $params['messages'] = "<p class='alert alert-danger'>Saldo anda Tidak Cukup</p>";
        return $params;
    }
    public function datappob(Request $request)
    {
        $from = date('Y-m-d');
        $until = $from;
        $stts = $request->status;
        if ($request->has('from') && $request->has('until')) {
          $from = date('Y-m-d',strtotime($request->from));
          $until = date('Y-m-d',strtotime($request->until));
        }
        $datas = Datatransaksippobs::whereBetWeen('tgl_trx',[$from,$until])->where('no_anggota',$request->user()->no_anggota)->where('status','LIKE','%'.$stts.'%')->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $from = date('d-m-Y',strtotime($from));
        $until = date('d-m-Y',strtotime($until));
        return view('pengurus.datadiri.dataPpob', compact('datas','from','until','stts'));
    }
    public function topup(Request $request)
    {
        $menus = Menutransaksi::where('aktif', 1)->orderBy('id', 'ASC')->get();
        $nominal= str_replace(".", "", $request->nominal);
        $uniks = substr($nominal, -4);
        $digit = substr($request->user()->no_anggota, -4);
        $total = 50000+$digit;
        // return $uniks.' '.$digit;

        if ($request->action =='topup') {
            if ($nominal < $total) {
                flash()->overlay('Minimal deposit 50,000 + '.$digit.' = '.number_format($total), 'GAGAL');
                return redirect()->back();
            }
            if ($uniks !== $digit) {
                flash()->overlay('Kode Unik tidak sesuai.', 'GAGAL');
                return redirect()->back();
            }
            if (Deposit::where('nominal', $nominal)->where('user_id', $request->user()->id)->where('bank', 'LIKE', '%'.$request->bank.'%')->where('status', 1)->first()) {
                flash()->overlay('Deposit sebelumnya masih dalam pengecekan.', 'GAGAL');
                return redirect()->back();
            }
            $banks = Bank::where('bank', $request->bank)->first();
            $create = Deposit::create([
              'user_id'=>$request->user()->id,
              'no_anggota'=>$request->user()->no_anggota,
              'no_trx'=>date('Ymd').rand(0, 100),
              'tgl_trx'=>date('Y-m-d'),
              'bank'=>$banks->bank.' : '.$banks->no_rekening,
              'nominal'=> $nominal,
              'status'=>1,
              'aktif'=>1,
              'created_by'=>$request->user()->id
            ]);
            if ($create) {
                flash()->overlay('Konfirmasi berhasil di kirim.', 'INFO');
                return redirect()->back();
            }
            flash()->overlay('Konfirmasi Gagal di kirim.', 'INFO');
            return redirect()->back();
        }
        $datas = Deposit::where('aktif', 1)->where('no_anggota', $request->user()->no_anggota)->orderBy('id', 'DESC')->get();
        return view('pengurus.datadiri.topup_deposit', compact('menus', 'datas'));
    }
    public function bankselect(Request $request)
    {
        if ($request->ajax()) {
            Log::info('BANK = '.$request->bank);
            $bankss = Bank::where('bank', $request->bank)->first();
            Log::info('$detail = '.$bankss);
            $option = "";
            $option.="<p>No. Rek : ".$bankss->no_rekening."  <br> Atas Nama : ".$bankss->atas_nama."</p>";
            return $option;
        }
    }
    public function saldotransaksi(Request $request)
    {
        $nomor = Auth::user()->no_anggota;
        $from = date('01-01-Y');
        $until = date('d-m-Y');
        $dari = date('Y-m-d', strtotime($from));
        $ke = date('Y-m-d', strtotime($until));
        if ($request->dari && $request->sampai) {
            $from = $request->dari;
            $until = $request->sampai;
            $dari = date('Y-m-d', strtotime($from));
            $ke = date('Y-m-d', strtotime($until));
        }
        $datasppob = Bukusaldotransaksi::whereBetWeen('tgl_trx', [$dari,$ke])->where('no_anggota', $nomor)->where('aktif', 1)->orderBy('created_at', 'ASC')->get();
        $users = User::find($request->user()->id);
        return view('pengurus.datadiri.bukusaldoTransaksi', compact('from', 'until', 'users', 'datasppob'));
    }
    public function tagihan(Request $request)
    {
        $menus = Menutransaksi::where('aktif', 1)->orderBy('id', 'ASC')->get();
        if ($request->ajax()) {
            if ($request['nomor']) {
                if ($request['nomor'] =="") {
                    $params['code'] = '400';
                    $params['messages'] = "<p class='alert alert-danger'>Isi form dengan lengkap</p>";
                    return $params;
                }
                $trxid = date('YmdHis').$request->user()->id;
                $params['trxid']=$trxid;
                $params['meteran']=$request['nomor'];
                $access_token = Website::find(1);
                  $url = $access_token->url.'cek-pln-pascabayar';
                $client = new \GuzzleHttp\Client();
                $res = $client->request('POST', $url, [
                            'headers'=>[
                                'Accept' => 'application/json',
                                'Content-Type' => 'application/json',
                                'Authorization' => 'Bearer '.$access_token->token
                            ],
                                  'body'=>json_encode($params)
                        ]);
                if ($res->getStatusCode() == 200) {
                    // return 'OK';
                    $hasil =  json_decode($res->getBody()->getContents(), true);
                    // return $hasil['meteran'];
                    if ($hasil['result'] =='success') {
                        $meteran = $hasil['meteran'];
                        $tagihan = $hasil['tagihan'];
                        $periode = $hasil['periode'];
                        $atas_nama = $hasil['atas_nama'];

                        $param['code'] = '200';
                        $param['messages'] = "<table class='table'>
                        <tr>
                          <td>Transaksi</td><td>:</td><td> PLN PASCABAYAR </td>
                        </tr>
                          <tr>
                            <td>ID Pelanggan</td><td>:</td><td> $meteran </td>
                          </tr>
                          <tr>
                            <td>Tagihan</td><td>:</td><td> $tagihan </td>
                          </tr>
                          <tr>
                            <td>Periode</td><td>:</td><td> $periode </td>
                          </tr>
                          <tr>
                            <td>Atas Nama</td><td>:</td><td> $atas_nama </td>
                          </tr>
                          <tr>
                            <td colspan='3' align='center'><br><a href=''><button class='btn'>History Pulsa</button></a></td>
                          </tr>
                      </table>";
                        return $param;
                    } else {
                        $param['code'] = '400';
                        $param['messages'] = "<p class='alert alert-danger'>". $hasil['message'] ."</p>";
                        return $param;
                    }
                }
            }
        }
        return view('pengurus.datadiri.pln_pascabayar', compact('menus'));
    }
    public function jumlah(Request $request)
    {
        if ($request->ajax()) {
            Log::info('Jumlah = '.$request->jumlah);
            $hargas = Toko::find($request->ids);
            $total_harga = $hargas->harga * $request->jumlah;
            Log::info('Data harga : '.$hargas->harga);
            $label ="";
            $label.="<input type='hidden' name='total_harga' value='".$total_harga."'/>";
            $label.="<label>Rp. ".number_format($hargas->harga * $request->jumlah).",-</label>";
            Log::info('TOTAL harga : '.$total_harga);
            return $label;
        }
    }
    public function province(Request $request)
    {
        if ($request->ajax()) {
            Log::info('ID province = '.$request->province);
            $citises = City::where('province_id', $request->province)->get();
            Log::info('$detail = '.$citises);
            $option = "";
            foreach ($citises as $key => $citys) {
                if ($key==0) {
                    $option.="<option value=''>Pilih Kota</option>";
                } else {
                    $option.="<option value='".$citys->city_id."'>".$citys->city_name."</option>";
                }
            }
            return $option;
        }
    }
    public function city(Request $request)
    {
        if ($request->ajax()) {
            Log::info('ID province = '.$request->city);
            $hargas = City::where('city_id', $request->city)->first();
            Log::info('alamat = '.$hargas);
            $total = $request['berat'] * $request['jumlah'];
            $kg = $total/1000;
            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://api.rajaongkir.com/starter/cost",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => "origin=106&destination=".$hargas->city_id."&weight=".$total."&courier=jne",
              CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded",
                "key:da08d414a043e0001fd33b5b3ad29ad0"
              ),
            ));
            $response = curl_exec($curl);
            Log::info('$response = '.$response);
            $resarr =json_decode($response, true);
            $jne =json_decode($response, true);
            $label ="";
            $label.="<input type='hidden' name='province' value='".$resarr['rajaongkir']['destination_details']['province']."'/>";
            $label.="<input type='hidden' name='city' value='".$resarr['rajaongkir']['destination_details']['city_name']."'/>";
            $label.="<input type='hidden' name='total_berat' value='".$total."'/>";
            $label.="<input type='hidden' name='ongkir' value='".$resarr['rajaongkir']['results'][0]['costs'][0]['cost'][0]['value']."'/>";
            $label.= "<p>".$resarr['rajaongkir']['results'][0]['name']."</p>";
            $label.= "<p>Total Berat : ".$total." Gram / ".$kg." Kg</p>";
            $label.= "<p>Ongkos Kirim Rp. ".number_format($resarr['rajaongkir']['results'][0]['costs'][0]['cost'][0]['value'])."</p>";
            $label.= "<p>Estimasi : ".$resarr['rajaongkir']['results'][0]['costs'][0]['cost'][0]['etd']." Hari </p>";
            return $label;
        }
    }

    public function kirim(Request $request)
    {
        if ($request->ajax()) {
            Log::info('ID province = '.$request->city);
            $hargas = City::where('city_id', $request->city)->first();
            Log::info('alamat = '.$hargas);
            $total = $request['berat'] * $request['jumlah'];
            $kg = $total/1000;
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.rajaongkir.com/starter/cost",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "origin=106&destination=".$hargas->city_id."&weight=".$total."&courier=".$request['kirim'],
                CURLOPT_HTTPHEADER => array(
                  "content-type: application/x-www-form-urlencoded",
                  "key:da08d414a043e0001fd33b5b3ad29ad0"
                ),
              ));
            $response = curl_exec($curl);
            Log::info('$response = '.$response);
            $resarr =json_decode($response, true);
            $jne =json_decode($response, true);
            $label ="";
            $label.="<input type='hidden' name='province' value='".$resarr['rajaongkir']['destination_details']['province']."'/>";
            $label.="<input type='hidden' name='city' value='".$resarr['rajaongkir']['destination_details']['city_name']."'/>";
            $label.="<input type='hidden' name='total_berat' value='".$total."'/>";
            $label.="<input type='hidden' name='ongkir' value='".$resarr['rajaongkir']['results'][0]['costs'][0]['cost'][0]['value']."'/>";
            $label.= "<p>".$resarr['rajaongkir']['results'][0]['name']."</p>";
            $label.= "<p>Total Berat : ".$total." Gram / ".$kg." Kg</p>";
            $label.= "<p>Ongkos Kirim Rp. ".number_format($resarr['rajaongkir']['results'][0]['costs'][0]['cost'][0]['value'])."</p>";
            $label.= "<p>Estimasi : ".$resarr['rajaongkir']['results'][0]['costs'][0]['cost'][0]['etd']." Hari </p>";
            return $label;
        }
    }
    public function bayar(Request $request)
    {
        if ($request->ids =="" || $request->berat =="" || $request->ongkir =="" || $request->jumlah =="" || $request->province =="" || $request->city =="" || $request->kirim =="" || $request->alamat =="") {
            flash()->overlay("<p class='alert alert-danger'>Data Harus Lengkap.</p>", "INFO");
            return redirect()->back();
        }
        if ($request->setuju =="") {
            flash()->overlay("<p class='alert alert-danger'>Anda belum Setujui debet saldo</p>", "INFO");
            return redirect()->back();
        }
        $kode = Toko::find($request->ids);
        $no_pesanan = date('ymdHis').$request->user()->id;
        $totalharga = (int)($kode->harga * $request->jumlah) + $request->ongkir;
        if ($request->user()->saldotransaksi < $totalharga) {
          $isisaldo = route('topup-deposit-anggota');
          flash()->overlay("<p class='alert alert-danger'>Saldo tidak cukup untuk transaksi.</p> <br><br> <a href=$isisaldo class='btn btn-primary'>Isi Saldo</a>", "INFO");
          return redirect()->back();
        }
        $saldo_akhir = $request->user()->saldotransaksi - $totalharga;
        $status = 2;
        $sisa = $kode->stok - $request->jumlah;
        if ($sisa < 0) {
          flash()->overlay("<p class='alert alert-danger'>Maaf Barang tersisa $kode->stok </p>", "INFO");
          return redirect()->back();
        }
        DB::beginTransaction();
        try {
            Toko::find($request->ids)->update([
              'stok'=>$sisa
            ]);
            User::find($request->user()->id)->update([
              'saldotransaksi'=>$saldo_akhir
            ]);
            Bukusaldotransaksi::create([
              'user_id'=>$request->user()->id,
              'no_anggota'=>$request->user()->no_anggota,
              'no_trx'=>date('ymdhis'),
              'tgl_trx'=>date('Y-m-d'),
              'nominal'=>$totalharga,
              'mutasi'=>'Debet',
              'keterangan'=>'Bayar '.$kode->kode.', no transaksi : '.$no_pesanan,
              'saldo'=>$saldo_akhir,
              'aktif'=>1,
            ]);
            Pesanan::create([
              'user_id'=>$request->user()->id,
              'no_anggota'=> $request->user()->no_anggota,
              'no_pemesanan'=> $no_pesanan,
              'tanggal'=> date("Y-m-d"),
              'kode'=>$kode->kode,
              'toko_id'=>$request->ids,
              'total_harga'=>$totalharga,
              'modal'=>$kode->modal,
              'berat'=>$request->berat,
              'ongkir'=>$request->ongkir,
              'jumlah'=>$request->jumlah,
              'propinsi'=>$request->province,
              'city'=>$request->city,
              'melalui'=>$request->kirim,
              'alamat'=>$request->alamat.', '.$request->city.', '.$request->province,
              'status'=>$status,
              'aktif'=>1,
              'created_by'=>$request->user()->id
            ]);
        } catch (\Exception $e) {
            Log::info('Gagal Transaksi:'.$e->getMessage());
            DB::rollback();
            flash()->overlay("<p class='alert alert-danger'>Gagal Transaksi.</p>", "INFO");
            return redirect()->back();
        }
        DB::commit();
        flash()->overlay("<p class='alert alert-success'>Transaksi berhasil.</p>", "INFO");
        return redirect()->route('pengurus-data-transaksi-belanja');
    }
    public function databelanja(Request $request)
    {
        $stts = $request->status;
        $today = date('Y-m-d');
        $from = $today;
        $until= $today;
        if ($request->from && $request->until) {
          $from = date('Y-m-d', strtotime($request->from));
          $until= date('Y-m-d', strtotime($request->until));
        }
        $datas = Pesanan::whereBetWeen('tanggal',[$from,$until])->where('status','LIKE','%'.$request->status.'%')->where('user_id',$request->user()->id)->orderBy('id','DESC')->get();
        $from = date('d-m-Y', strtotime($from));
        $until= date('d-m-Y', strtotime($until));
        $statuses = Status::whereBetween('id',[2,5])->get();
        return view('pengurus.datadiri.laporan_belanja', compact('datas','from','until','stts','statuses'));
    }
}
