<?php

namespace App\Http\Controllers\Pengurus;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pendidikan;
use App\Komunitas;
use App\Kelurahan;
use App\Kecamatan;
use App\Propinsi;
use App\Pendapatan;
use App\Pekerjaan;
use App\JenisSimpanan;
use App\Kabupaten;
use App\Bank;

class DataMasterController extends Controller
{
  public function __construct()
  {
      $this->middleware('pengurus');
  }
  public function pendidikan(Request $request){
    $pendidikans = Pendidikan::where('aktif',1)->get();
    return view('pengurus.dataMaster.dataPendidikan',compact('pendidikans'));
  }
  public function komunitas(Request $request){
    $komunitass = Komunitas::where('aktif',1)->get();
    return view('pengurus.dataMaster.dataKomunitas',compact('komunitass'));
  }
  public function kelurahan(Request $request){
    $kelurahans = Kelurahan::where('aktif',1)->get();
    return view('pengurus.dataMaster.dataKelurahan',compact('kelurahans'));
  }
  public function kecamatan(Request $request){
    $kecamatans = Kecamatan::where('aktif',1)->get();
    return view('pengurus.dataMaster.dataKecamatan',compact('kecamatans'));
  }
  public function kabupaten(Request $request){
    $kabupatens = Kabupaten::where('aktif',1)->get();
    return view('pengurus.dataMaster.dataKabupaten',compact('kabupatens'));
  }
  public function propinsi(Request $request){
    $propinsis = Propinsi::where('aktif',1)->get();
    return view('pengurus.dataMaster.dataPropinsi',compact('propinsis'));
  }
  public function pendapatan(Request $request){
    $pendapatans = Pendapatan::where('aktif',1)->get();
    return view('pengurus.dataMaster.dataPendapatan',compact('pendapatans'));
  }
  public function pekerjaan(Request $request){
    $pekerjaans = pekerjaan::where('aktif',1)->get();
    return view('pengurus.dataMaster.dataPekerjaan',compact('pekerjaans'));
  }
  public function jenissimpanan(Request $request){
    $jenissimpanans = jenissimpanan::where('aktif',1)->get();
    return view('pengurus.dataMaster.jenisSimpanan',compact('jenissimpanans'));
  }
  public function rekening(Request $request)
  {
      $rekenings = Bank::where('aktif', 1)->get();
      return view('pengurus.dataMaster.dataBank', compact('rekenings'));
  }
}
