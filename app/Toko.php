<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Toko extends Model
{
  protected $fillable = [
    'id','kode','name','harga','modal','berat','aktif','gambar','keterangan','stok','admin','created_at','created_by','updated_at','updated_by','deteted_at','deleted_by'
  ];
}
